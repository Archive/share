
/*  $Header$
 *
 *  Copyright (c) 2003 Sun Microsystems, Inc.
 *  All Rights Reserved.
 */

2003-10-28 Rich Burridge <rich.burridge@sun.com>

    * Added a simple chat_server example.

    * Removed:

      share_byte_array_listener.[c,h]
      share_channel_listener.[c,h]
      share_client_listener.[c,h]
      share_connection_listener.[c,h]
      share_listener.[c,h]
      share_registry_listener.[c,h]
      share_session_listener.[c,h]
      share_token_listener.[c,h]

      The signals on the relevant objects will now be used instead.

    * Removed:

      ShareStatus share_channel_add_listener();
      ShareStatus share_channel_remove_listener();

      ShareStatus share_connection_add_listener();
      ShareStatus share_connection_remove_listener();

      ShareStatus share_registry_factory_add_listener();
      ShareStatus share_registry_factory_remove_listener();

      ShareStatus share_session_add_listener();
      ShareStatus share_session_remove_listener();

      ShareStatus share_token_add_listener();
      ShareStatus share_token_remove_listener();

    * Removed:

        ShareUrlString *share_url_string_new(gchar *url);

      from the share_url_string.h file. 

      The routine:

        ShareUrlString *share_url_string_create_session_url()

      is now called:

        ShareUrlString *share_url_string_new_session_url();

      The routine:

        ShareUrlString *share_url_string_create_client_url();

      is now called:

        ShareUrlString *share_url_string_new_client_url();

    * The following four routines shouldn't have an initial 
      ShareRegistryFactory * parameter:

        ShareStatus share_registry_factory_registry_exists();

        ShareStatus share_registry_factory_registry_exists_with_port();

        ShareStatus share_registry_factory_list();

        ShareStatus share_registry_factory_list_on_host();

    * The routine:

        ShareRegistryFactory * share_registry_factory_new();

      is now:

        ShareRegistry * share_registry_new(gchar *registry_type);

      in share_registry.[c,h]

      Added:

        ShareRegistry * share_registry_new_with_port(gchar *registry_type,
                                                     gint port);

        ShareRegistry * share_registry_new_with_manager(gchar *registry_type,
                                                        ShareManager *manager);

      in share_registry.[c,h].

      The three routines:

        ShareStatus share_registry_factory_start_registry(
                                        ShareRegistryFactory *registry_factory,
                                        gchar *registry_type);

        ShareStatus share_registry_factory_start_registry_with_port(
                                        ShareRegistryFactory *registry_factory,
                                        gchar *registry_type,
                                        gint port);

        ShareStatus share_registry_factory_start_registry_with_manager
                                       (ShareRegistryFactory *registry_factory,
                                        gchar *registry_type,
                                        ShareManager *manager);

      now become:

        ShareStatus share_registry_start_registry(ShareRegistry *registry);

      in share_registry.[c,h].

      The two routines:

        ShareStatus
        share_registry_factory_stop_registry(
                                        ShareRegistryFactory *registry_factory,
                                        gchar *registry_type);

        ShareStatus
        share_registry_factory_stop_registry_with_port
                                       (ShareRegistryFactory *registry_factory,
                                        gchar *registry_type,
                                        gint port);

      now become:

        ShareStatus share_registry_stop_registry(ShareRegistry *registry);

      in share_registry.[c,h].

    * Removed:

        ShareSessionFactory *share_session_factory_new();

      The routine:

        ShareStatus share_session_factory_create_session(
                                        ShareSessionFactory *session_factory,
                                        ShareClient *client,
                                        ShareUrlString *url_string,
                                        gboolean auto_join,
                                        ShareSession *session);

      now becomes:

        ShareSession *share_session_new_session(ShareClient *client,
                                        ShareUrlString *url_string,
                                        gboolean auto_join);
      in share_session.[c,h]

      The routine:

        ShareStatus share_session_factory_create_session_with_manager(
                                        ShareSessionFactory *session_factory,
                                        ShareClient *client,
                                        ShareUrlString *url_string,
                                        gboolean auto_join,
                                        ShareSessionManager *manager,
                                        ShareSession *session);

      now becomes:

        ShareStatus share_session_new_session_with_manager(
                                        ShareClient *client,
                                        ShareUrlString *url_string,
                                        gboolean auto_join,
                                        ShareSessionManager *manager);

      in share_session.[c,h]

    * Removed:

        ShareStatus
        share_manageable_enable_listener_events  (ShareManageable *manageable,
                                                  ShareListener *listener,
                                                  gint event_mask);

        ShareStatus
        share_manageable_disable_listener_events (ShareManageable *manageable,
                                                  ShareListener *listener,
                                                  gint event_mask);

      from share_manageable.[c,h]

2003-10-24 Rich Burridge <rich.burridge@sun.com>

    * Fixups to use GSignal where possible.
    * Removed <para>...</para> tags, which aren't really needed by gtk-doc.

2003-09-17 Rich Burridge <rich.burridge@sun.com>

    * Adjustments to allow the build to successfully complete.

2003-09-15 Rich Burridge <rich.burridge@sun.com>

    * From Glynn Foster <Glynn.Foster@sun.com>
      Suggested changes to try to fix the build recursion problem.

2003-09-05 Rich Burridge <rich.burridge@sun.com>

    * Added share_<whatever>_class_init() and share_<whatever>_instance_init()
      routines to each .c file.

    * Started adjusting share_byte_array.[c,h] to use GSignal to generate
      the five types of ShareByteArray events.

2003-08-28 Rich Burridge <rich.burridge@sun.com>

    * Fixed up the "No template matches code" messages generated by 
      gtkdoc-mkhtml, by adding <itemizedlist>, </itemizedlist>,
      <listitem>, </listitem> , <para> and </para> tags as appropriate
      to replace the HTML tags.

2003-08-28 Rich Burridge <rich.burridge@sun.com>

    * Fixed up all the warning messages generated by gtkdoc-mkdb.

2003-08-22 Rich Burridge <rich.burridge@sun.com>

    * First cut at adjusting the comments from JavaDoc to gtk-doc style.
    * Added the appropriate configure.in and Makefile.am rules for gtk-doc
      generation.

2003-08-21 Rich Burridge <rich.burridge@sun.com>

    * Fixed up the return type and/or parameter usage for various routines.
    * Better indented the various entries in the .h files. Removed the
      "Can return:" comments.

2003-08-19 Rich Burridge <rich.burridge@sun.com>

    * Added in JavaDoc style comments for all of the Share source files.
    * Fixed up 13 of 38 of these source files so that they now reflect the
      C-style API rather than the original Java style API (from where 
      they were taken).
    * Fixed up the return type and/or parameter usage for various routines.

2003-08-14 Rich Burridge <rich.burridge@sun.com>

    * Initial version for GNOME CVS.

2003-08-13 Rich Burridge <rich.burridge@sun.com>

    * Created a first cut at a C-style API to the Java Shared Data API.
      At this point there are header files and .c stubs for all the 
      routines and they compile. No real comments added to the files 
      yet though.
