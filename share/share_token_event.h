
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_TOKEN_EVENT_H__
#define __SHARE_TOKEN_EVENT_H__

#include <share/share_event.h>
#include <share/share_token.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_TOKEN_EVENT \
            (share_token_event_get_type())

#define SHARE_TOKEN_EVENT(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_TOKEN_EVENT, ShareTokenEvent))

#define SHARE_TOKEN_EVENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_TOKEN_EVENT, ShareTokenEventClass))

#define SHARE_IS_TOKEN_EVENT(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_TOKEN_EVENT))

#define SHARE_IS_TOKEN_EVENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_TOKEN_EVENT))

#define SHARE_TOKEN_EVENT_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_TOKEN_EVENT, ShareTokenEventClass))

typedef struct _ShareTokenEventClass   ShareTokenEventClass;

struct _ShareTokenEvent
{
    ShareEvent event;
};

struct _ShareTokenEventClass
{
    ShareEventClass parent_class;
};


/** The Token given event type. */
static int SHARE_TOKEN_GIVEN     = 1 << 0;

/** The Token grabbed event type. */
static int SHARE_TOKEN_GRABBED   = 1 << 1;

/** The Token inhibited event type. */
static int SHARE_TOKEN_INHIBITED = 1 << 2;

/** The Token joined event type. */
static int SHARE_TOKEN_JOINED    = 1 << 3;

/** The Token left event type. */
static int SHARE_TOKEN_LEFT      = 1 << 4;

/** The Token released event type. */
static int SHARE_TOKEN_RELEASED  = 1 << 5;

/** The Token requested event type. */
static int SHARE_TOKEN_REQUESTED = 1 << 6;

/** The Token invited event type. */
static int SHARE_TOKEN_INVITED   = 1 << 7;

/** The Token expelled event type. */
static int SHARE_TOKEN_EXPELLED  = 1 << 8;

GType 
share_token_event_get_type             (void);

ShareTokenEvent * 
share_token_event_new                  (ShareSession *session,
                                        gchar *client_name,
                                        ShareToken *token,
                                        gint type);

gchar * 
share_token_event_get_client_name      (ShareTokenEvent *event);

ShareSession * 
share_token_event_get_session          (ShareTokenEvent *event);

ShareToken * 
share_token_event_get_token            (ShareTokenEvent *event);

gchar * 
share_token_event_to_string            (ShareTokenEvent *event);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_TOKEN_EVENT_H__ */
