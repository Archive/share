
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_CHANNEL_EVENT_H__
#define __SHARE_CHANNEL_EVENT_H__

#include <share/share_event.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_CHANNEL_EVENT \
            (share_channel_event_get_type())

#define SHARE_CHANNEL_EVENT(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_CHANNEL_EVENT, ShareChannelEvent))

#define SHARE_CHANNEL_EVENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_CHANNEL_EVENT, ShareChannelEventClass))

#define SHARE_IS_CHANNEL_EVENT(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_CHANNEL_EVENT))

#define SHARE_IS_CHANNEL_EVENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_CHANNEL_EVENT))

#define SHARE_CHANNEL_EVENT_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_CHANNEL_EVENT, ShareChannelEventClass))

typedef struct _ShareChannelEvent        ShareChannelEvent;
typedef struct _ShareChannelEventClass   ShareChannelEventClass;

struct _ShareChannelEvent
{
    ShareEvent event;
};

struct _ShareChannelEventClass
{
    ShareEventClass parent_class;
};


/** The Channel joined event type. */
static int SHARE_CHANNEL_JOINED           = 1 << 0;

/** The Channel left event type. */
static int SHARE_CHANNEL_LEFT             = 1 << 1;

/** The Channel invited event type. */
static int SHARE_CHANNEL_INVITED          = 1 << 2;

/** The Channel expelled event type. */
static int SHARE_CHANNEL_EXPELLED         = 1 << 3;

/** The Channel consumer added event type. */
static int SHARE_CHANNEL_CONSUMER_ADDED   = 1 << 4;

/** The Channel consumer removed event type. */
static int SHARE_CHANNEL_CONSUMER_REMOVED = 1 << 5;

GType 
share_channel_event_get_type        (void);

ShareChannelEvent * 
share_channel_event_new             (ShareSession *session,
                                     gchar *client_name,
                                     ShareChannel *channel,
                                     gint type);

ShareChannel * 
share_channel_event_get_channel     (ShareChannelEvent *event);

gchar * 
share_channel_event_get_client_name (ShareChannelEvent *event);

ShareSession * 
share_channel_event_get_session     (ShareChannelEvent *event);

gchar * 
share_channel_event_to_string       (ShareChannelEvent *event);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_CHANNEL_EVENT_H__ */
