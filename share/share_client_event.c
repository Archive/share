
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The Client event. Client events are created for the following actions:
 * <itemizedlist>
 *   <listitem>when a Client has been invited to join a ByteArray.</listitem>
 *   <listitem>when a Client has been expelled from a ByteArray.</listitem>
 *   <listitem>when a Client has been invited to join a Channel.</listitem>
 *   <listitem>when a Client has been expelled from a Channel.</listitem>
 *   <listitem>when a Client has been invited to join a Session.</listitem>
 *   <listitem>when a Client has been expelled from a Session.</listitem>
 *   <listitem>when a Client has been invited to join a Token.</listitem>
 *   <listitem>when a Client has been expelled from a Token.</listitem>
 *   <listitem>when a Client has been given a Token.</listitem>
 * </itemizedlist>
 */

#include <share/share_client_event.h>

static void share_client_event_class_init     (ShareClientEventClass *class);
static void share_client_event_instance_init  (ShareClientEvent	*instance);


GType 
share_client_event_get_type(void)
{
    printf("share_client_event_get_type called.\n");
}


static void 
share_client_event_class_init(ShareClientEventClass *class)
{
    printf("share_client_event_class_init called.\n");
}


static void 
share_client_event_instance_init(ShareClientEvent *instance)
{
    printf("share_client_event_instance_init called.\n");
}


/**
 * share_client_event_new:
 * @session: the session that this client event is associated with.
 * @client: the Client associated with this Client event.
 * @resource_name: the name of the ByteArray/Channel/Session/Token. 
 * @type: the type of event.
 *
 * Constructor for the ClientEvent object. A new client event is generated
 * for a specific client when there has been a change in the clients status
 * (when it's been invited to join, or expelled from a ByteArray, Channel,
 * Session or Token).
 *
 * Returns: a ShareClientEvent object.
 */

ShareClientEvent * 
share_client_event_new(ShareSession *session,
                       ShareClient *client,
                       gchar *resource_name,
                       gint type)
{
    printf("share_client_event_new called.\n");
}


/**
 * share_client_event_get_resource_name:
 * @event: the Client event.
 *
 * Get the name of the resource for this event. The resource will be the
 * ByteArray, Channel, Session or Token that the Client has been invited
 * to join or has been expelled from.
 *
 * Returns: the name of the resource for this event.
 */

gchar * 
share_client_event_get_resource_name(ShareClientEvent *event)
{
    printf("share_client_event_get_resource_name called.\n");
}


/**
 * share_client_event_get_session:
 * @event: the Client event.
 *
 * Get the Session associated with this event.
 *
 * Returns: the Session associated with this event.
 */

ShareSession * 
share_client_event_get_session(ShareClientEvent *event)
{
    printf("share_client_event_get_session called.\n");
}


/**
 * share_client_event_to_string:
 * @event: the Client event.
 *
 * Return a short description of this Client event.
 *
 * Returns: a string containing a description of this Client event.
 */

gchar * 
share_client_event_to_string(ShareClientEvent *event)
{
    printf("share_client_event_to_string called.\n");
}
