
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The Connection object.
 */

#include <share/share_connection.h>
#include <share/share_marshal.h>

enum {
    SHARE_CONNECTION_FAILED_SIGNAL,
    SHARE_CONNECTION_LAST_SIGNAL
};

static void share_connection_class_init	    (ShareConnectionClass *class);
static void share_connection_instance_init  (ShareConnection *instance);


static guint connection_signals[SHARE_CONNECTION_LAST_SIGNAL] = { 0 };
static ShareObjectClass *parent_class = NULL;


GType 
share_connection_get_type(void)
{
    static GType connection_type = 0;

    if (!connection_type) {
	static const GTypeInfo connection_info = {
            sizeof(ShareConnectionClass),
            NULL,			/* base_init */
            NULL,			/* base_finalize */
            (GClassInitFunc) share_connection_class_init,
            NULL,			/* class_finalize */
            NULL,			/* class_data */
            sizeof(ShareConnection),
            0,				/* n_preallocs */
            (GInstanceInitFunc) share_connection_instance_init,
	};

	connection_type = g_type_register_static(SHARE_TYPE_OBJECT,
                                                 "ShareConnection",
                                                 &connection_info, 0);
    }

    return(connection_type);
}


static void 
share_connection_class_init(ShareConnectionClass *class)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS(class);

    parent_class = g_type_class_peek_parent(class);

    connection_signals[SHARE_CONNECTION_FAILED_SIGNAL] =
	g_signal_new("share_connection_failed",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareConnectionClass,
                            share_connection_failed),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CONNECTION,
            SHARE_TYPE_CONNECTION_EVENT);
}


static void 
share_connection_instance_init(ShareConnection *instance)
{
    printf("share_connection_instance_init called.\n");
}


ShareConnection * 
share_connection_new(void)
{
    ShareConnection *connection;

    connection = g_object_new(share_connection_get_type(), NULL);

    return(connection);
}


/**
 * share_connection_get_properties:
 * @connection: the Connection.
 *
 * Get the current Share property list. These property values override
 * the default values in #share_object and are used by each
 * Share implementation. Initially this property set is empty.
 *
 * Information of the specific properties used by each implementation can
 * be found in the Implementations chapter in the Share User Guide.
 *
 * Returns: the current Share property list.
 */

ShareProperties * 
share_connection_get_properties(ShareConnection *connection)
{
    printf("share_connection_get_properties called.\n");
}


/**
 * share_connection_get_property:
 * @connection: the Connection.
 * @key: the property key.
 *
 * Searches for the implementation property with the specified key in the
 * Share property list. If the key is not found in this property list, then
 * the default value (from #share_object is used by the implementation.
 *
 * Information of the specific properties used by each implementation can
 * be found in the Implementations chapter in the Share User Guide.
 *
 * Returns: the value in this property list with the specified key value or
 *          %NULL if the property is not found.
 */

gchar * 
share_connection_get_property(ShareConnection *connection,
                              gchar *key)
{
    printf("share_connection_get_property called.\n");
}


/**
 * share_connection_set_properties:
 * @connection: the Connection.
 * @properties: the new Share property set to use.
 *
 * Replaces the current Share property list with the given set of properties.
 * These property values will be used by the implementation instead of the
 * default values in #share_object.
 *
 * Information of the specific properties used by each implementation can
 * be found in the Implementations chapter in the Share User Guide.
 */

void 
share_connection_set_properties(ShareConnection *connection,
                                ShareProperties *properties)
{
    printf("share_connection_set_properties called.\n");
}


/**
 * share_connection_set_property:
 * @connection: the Connection.
 * @key: the property key.
 * @value: the value to associate with this key.
 *
 * Sets an implementation property with the specified key in the Share
 * property list. This property value will override it's equivalent value
 * in #share_object and be used by the implementation.
 *
 * Information of the specific properties used by each implementation can
 * be found in the Implementations chapter in the Share User Guide.
 *
 * Returns: the previous value of the specified key in this hashtable,
 *          or %NULL if it did not have one.
 */

GObject * 
share_connection_set_property(ShareConnection *connection,
                              gchar *key,
                              gchar *value)
{
    printf("share_connection_set_property called.\n");
}


/**
 * share_connection_remove_property:
 * @connection: the Connection.
 * @key: the property key.
 *
 * Removes an implementation property with the specified key from the Share
 * property list. Unless this property is reset into the property list, the
 * default value in #share_object will be used by the implementation.
 *
 * Information of the specific properties used by each implementation can
 * be found in the Implementations chapter in the Share User Guide.
 *
 * Returns: the value to which the key had been mapped in this hashtable,
 *          or %NULL if the key did not have a mapping.
 */

GObject * 
share_connection_remove_property(ShareConnection *connection,
                                 gchar *key)
{
    printf("share_connection_remove_property called.\n");
}
