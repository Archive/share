
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The Token event. Token events are created for the following actions:
 * <itemizedlist>
 *   <listitem>when a Token has been given from one Client to another.</listitem>
 *   <listitem>when a Client has grabbed a Token.</listitem>
 *   <listitem>when a Client has inhibited a Token.</listitem>
 *   <listitem>when a Client has joined a Token.</listitem>
 *   <listitem>when a Client has left a Token.</listitem>
 *   <listitem>when a Client has released itself from a Token.</listitem>
 *   <listitem>when a Client has requested a Token.</listitem>
 *   <listitem>when a Client has been invited to join a Token.</listitem>
 *   <listitem>when a Client has been expelled from a Token.</listitem>
 * </itemizedlist>
 */

#include <share/share_token_event.h>

static void share_token_event_class_init     (ShareTokenEventClass *class);
static void share_token_event_instance_init  (ShareTokenEvent *instance);


GType 
share_token_event_get_type(void)
{
    printf("share_token_event_get_type called.\n");
}


static void 
share_token_event_class_init(ShareTokenEventClass *class)
{
    printf("share_token_event_class_init called.\n");
}


static void 
share_token_event_instance_init(ShareTokenEvent *instance)
{
    printf("share_token_event_instance_init called.\n");
}


/**
 * share_token_event_new:
 * @session: the session this channel belongs to.
 * @client_name: the name of the client.
 * @token: the token.
 * @type: the type of event.
 *
 * Constructor for the TokenEvent class. A new token event is generated for
 * a client action for a specific token.
 *
 * Returns: a ShareTokenEvent object.
 */

ShareTokenEvent * 
share_token_event_new(ShareSession *session,
                      gchar *client_name,
                      ShareToken *token,
                      gint type)
{
    printf("share_token_event_new called.\n");
}


/**
 * share_token_event_get_client_name:
 * @event: the Token event.
 *
 * Get the name of the Client that generated this event.
 *
 * Returns: the name of the Client that generated this event.
 */

gchar * 
share_token_event_get_client_name(ShareTokenEvent *event)
{
    printf("share_token_event_get_client_name called.\n");
}


/**
 * share_token_event_get_session:
 * @event: the Token event.
 *
 * Get the Session associated with this event.
 *
 * Returns: the Session associated with this event.
 */

ShareSession * 
share_token_event_get_session(ShareTokenEvent *event)
{
    printf("share_token_event_get_session called.\n");
}


/**
 * share_token_event_get_token:
 * @event: the Token event.
 *
 * Get the name of the token for this event.
 *
 * Returns: the name of the token for this event.
 */

ShareToken * 
share_token_event_get_token(ShareTokenEvent *event)
{
    printf("share_token_event_get_token called.\n");
}


/**
 * share_token_event_to_string:
 * @event: the Token event.
 *
 * Return a short description of this Token event.
 *
 * Returns: a String containing a description of this Token event.
 */

gchar * 
share_token_event_to_string(ShareTokenEvent *event)
{
    printf("share_token_event_to_string called.\n");
}
