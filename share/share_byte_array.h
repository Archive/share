
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_BYTE_ARRAY_H__
#define __SHARE_BYTE_ARRAY_H__

#include <share/share_manageable.h>
#include <share/share_byte_array_event.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_BYTE_ARRAY \
            (share_byte_array_get_type())

#define SHARE_BYTE_ARRAY(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_BYTE_ARRAY, ShareByteArray))

#define SHARE_BYTE_ARRAY_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_BYTE_ARRAY, ShareByteArrayClass))

#define SHARE_IS_BYTE_ARRAY(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_BYTE_ARRAY))

#define SHARE_IS_BYTE_ARRAY_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_BYTE_ARRAY))

#define SHARE_BYTE_ARRAY_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_BYTE_ARRAY, ShareByteArrayClass))

typedef struct _ShareByteArrayClass ShareByteArrayClass;

struct _ShareByteArray
{
    ShareManageable manageable;
};

struct _ShareByteArrayClass
{
    ShareManageableClass parent_class;

    void (* share_byte_array_joined)        (ShareByteArray *byte_array,
                                             ShareByteArrayEvent *event);
    void (* share_byte_array_left)          (ShareByteArray *byte_array,
                                             ShareByteArrayEvent *event);
    void (* share_byte_array_value_changed) (ShareByteArray *byte_array,
                                             ShareByteArrayEvent *event);
    void (* share_byte_array_invited)       (ShareByteArray *byte_array,
                                             ShareByteArrayEvent *event);
    void (* share_byte_array_expelled)      (ShareByteArray *byte_array,
                                             ShareByteArrayEvent *event);
};


GType
share_byte_array_get_type                   (void);

ShareByteArray * 
share_byte_array_new                        (void);

ShareStatus 
share_byte_array_get_value_as_bytes         (ShareByteArray *byte_array,
                                             byte *value[]);

ShareStatus 
share_byte_array_get_value_as_object        (ShareByteArray *byte_array,
                                             GObject *object);
 
ShareStatus 
share_byte_array_get_value_as_string        (ShareByteArray *byte_array,
                                             gchar *string);
 
ShareStatus 
share_byte_array_set_value                  (ShareByteArray *byte_array,
                                             ShareClient *client,
                                             byte *value[]);
 
ShareStatus 
share_byte_array_set_value_with_offset_and_length
                                            (ShareByteArray *byte_array,
                                             ShareClient *client,
                                             byte *value[],
                                             gint offset,
                                             gint length);
 
ShareStatus 
share_byte_array_set_value_from_string      (ShareByteArray *byte_array,
                                             ShareClient *client,
                                             gchar *string);
 
ShareStatus 
share_byte_array_set_value_from_object      (ShareByteArray *byte_array,
                                             ShareClient *client,
                                             GObject *object);
 
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_BYTE_ARRAY_H__ */
