
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The Client object.
 */

#include <share/share_client.h>
#include <share/share_marshal.h>

enum {
    SHARE_BYTE_ARRAY_EXPELLED_SIGNAL,
    SHARE_BYTE_ARRAY_INVITED_SIGNAL,
    SHARE_CHANNEL_EXPELLED_SIGNAL,
    SHARE_CHANNEL_INVITED_SIGNAL,
    SHARE_SESSION_EXPELLED_SIGNAL,
    SHARE_SESSION_INVITED_SIGNAL,
    SHARE_TOKEN_EXPELLED_SIGNAL,
    SHARE_TOKEN_GIVEN_SIGNAL,
    SHARE_TOKEN_INVITED_SIGNAL,
    SHARE_CLIENT_LAST_SIGNAL
};

static void share_client_class_init	(ShareClientClass *class);
static void share_client_instance_init  (ShareClient      *instance);


static guint client_signals[SHARE_CLIENT_LAST_SIGNAL] = { 0 };
static ShareObjectClass *parent_class = NULL;


GType    
share_client_get_type(void)
{
    static GType client_type = 0;

    if (!client_type) {
	static const GTypeInfo client_info = {
            sizeof(ShareClientClass),
            NULL,			/* base_init */
            NULL,			/* base_finalize */
            (GClassInitFunc) share_client_class_init,
            NULL,			/* class_finalize */
            NULL,			/* class_data */
            sizeof(ShareClient),
            0,				/* n_preallocs */
            (GInstanceInitFunc) share_client_instance_init,
	};

	client_type = g_type_register_static(SHARE_TYPE_OBJECT,
                                             "ShareClient",
                                             &client_info, 0);
    }

    return(client_type);
}


static void 
share_client_class_init(ShareClientClass *class)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS(class);

    parent_class = g_type_class_peek_parent(class);

    client_signals[SHARE_BYTE_ARRAY_EXPELLED_SIGNAL] =
	g_signal_new("share_byte_array_expelled",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareClientClass,
                            share_byte_array_expelled),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CLIENT,
            SHARE_TYPE_CLIENT_EVENT);

    client_signals[SHARE_BYTE_ARRAY_INVITED_SIGNAL] =
        g_signal_new("share_byte_array_invited",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareClientClass,
                            share_byte_array_invited),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CLIENT,
            SHARE_TYPE_CLIENT_EVENT);

    client_signals[SHARE_CHANNEL_EXPELLED_SIGNAL] =
        g_signal_new("share_channel_expelled",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareClientClass,
                            share_channel_expelled),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CLIENT,
            SHARE_TYPE_CLIENT_EVENT);

    client_signals[SHARE_CHANNEL_INVITED_SIGNAL] =
        g_signal_new("share_channel_invited",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareClientClass,
                            share_channel_invited),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CLIENT,
            SHARE_TYPE_CLIENT_EVENT);

    client_signals[SHARE_SESSION_EXPELLED_SIGNAL] =
        g_signal_new("share_session_expelled",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareClientClass,
                            share_session_expelled),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CLIENT,
            SHARE_TYPE_CLIENT_EVENT);

    client_signals[SHARE_SESSION_INVITED_SIGNAL] =
        g_signal_new("share_session_invited",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareClientClass,
                            share_session_invited),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CLIENT,
            SHARE_TYPE_CLIENT_EVENT);

    client_signals[SHARE_TOKEN_EXPELLED_SIGNAL] =
        g_signal_new("share_token_expelled",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareClientClass,
                            share_token_expelled),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CLIENT,
            SHARE_TYPE_CLIENT_EVENT);

    client_signals[SHARE_TOKEN_GIVEN_SIGNAL] =
        g_signal_new("share_token_given",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareClientClass,
                            share_token_given),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CLIENT,
            SHARE_TYPE_CLIENT_EVENT);

    client_signals[SHARE_TOKEN_INVITED_SIGNAL] =
        g_signal_new("share_token_invited",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareClientClass,
                            share_token_invited),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CLIENT,
            SHARE_TYPE_CLIENT_EVENT);
}


static void 
share_client_instance_init(ShareClient *instance)
{
    printf("share_client_instance_init called.\n");
}


ShareClient * 
share_client_new(gchar *name)
{
    ShareClient *client;

    client = g_object_new(share_client_get_type(), NULL);

    return(client);
}


/**
 * share_client_authenticate:
 * @client: the Client.
 * @info: the authentication information.
 *
 * Used to authenticate a Client for potentially joining a managed object,
 * or creating or destroying a ByteArray, Channel or Token within a managed
 * Session. The ByteArray, Channel, Session or Token manager will be doing
 * this Client validation.
 *
 * The manager sends the Client an authentication request. Within this
 * request is a challenge. The Client replies with a response.
 * This response is validated by the manager and determines if the
 * Client will be allowed to join the ByteArray/Channel/Session/Token
 * or create/destroy the ByteArray/Channel/Token.
 *
 * The challenge given by the manager and the response provided by the
 * Client are both GObjects. There must be some agreed policy between
 * the manager and the Client with regards to these objects. In other words
 * the Client needs to know what to do with the challenge and how to respond
 * to it, and the manager needs to know how to handle that response.
 *
 * Returns: the response by the Client to the managers challenge.
 */

GObject * 
share_client_authenticate(ShareClient *client,
                          ShareAuthenticationInfo *info)
{
    printf("share_client_authenticate called.\n");
}


/**
 * share_client_get_name:
 * @client: the Client.
 *
 * Get the name of this Client.
 *
 * Returns: the name of the Client.
 */

gchar * 
share_client_get_name(ShareClient *client)
{
    printf("share_client_get_name called.\n");
}
