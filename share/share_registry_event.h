
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_REGISTRY_EVENT_H__
#define __SHARE_REGISTRY_EVENT_H__

#include <share/share_event.h>
#include <share/share_url_string.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_REGISTRY_EVENT \
            (share_registry_event_get_type())

#define SHARE_REGISTRY_EVENT(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_REGISTRY_EVENT, ShareRegistryEvent))

#define SHARE_REGISTRY_EVENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_REGISTRY_EVENT, ShareRegistryEventClass))

#define SHARE_IS_REGISTRY_EVENT(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_REGISTRY_EVENT))

#define SHARE_IS_REGISTRY_EVENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_REGISTRY_EVENT))

#define SHARE_REGISTRY_EVENT_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_REGISTRY_EVENT, ShareRegistryEventClass))

typedef struct _ShareRegistryEvent        ShareRegistryEvent;
typedef struct _ShareRegistryEventClass   ShareRegistryEventClass;

struct _ShareRegistryEvent
{
    ShareEvent event;
};

struct _ShareRegistryEventClass
{
    ShareEventClass parent_class;
};


/** The Registry connection failed event type. */
static int SHARE_REGISTRY_CONNECTION_FAILED = 1 << 0;

/** The Registry Session created event type. */
static int SHARE_REGISTRY_SESSION_CREATED   = 1 << 1;

/** The Registry Session destroyed event type. */
static int SHARE_REGISTRY_SESSION_DESTROYED = 1 << 2;

/** The Registry Client created event type. */
static int SHARE_REGISTRY_CLIENT_CREATED    = 1 << 3;

/** The Registry Client destroyed event type. */
static int SHARE_REGISTRY_CLIENT_DESTROYED  = 1 << 4;

GType 
share_registry_event_get_type             (void);

ShareRegistryEvent * 
share_registry_event_new                  (gchar *client_name,
                                           ShareUrlString *resource_name,
                                           gchar *address,
                                           gint port,
                                           gint type);

gchar * 
share_registry_event_get_address          (ShareRegistryEvent *event);

gchar * 
share_registry_event_get_client_name      (ShareRegistryEvent *event);

gint 
share_registry_event_get_port             (ShareRegistryEvent *event);

ShareUrlString * 
share_registry_event_get_resource_name    (ShareRegistryEvent *event);

gchar * 
share_registry_event_to_string            (ShareRegistryEvent *event);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_REGISTRY_EVENT_H__ */
