
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.          See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_CHANNEL_CONSUMER_H__
#define __SHARE_CHANNEL_CONSUMER_H__

#include <share/share_object.h>
#include <share/share_data.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_CHANNEL_CONSUMER \
            (share_channel_consumer_get_type())

#define SHARE_CHANNEL_CONSUMER(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_CHANNEL_CONSUMER, ShareChannelConsumer))

#define SHARE_CHANNEL_CONSUMER_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_CHANNEL_CONSUMER, ShareChannelConsumerClass))

#define SHARE_IS_CHANNEL_CONSUMER(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_CHANNEL_CONSUMER))

#define SHARE_IS_CHANNEL_CONSUMER_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_CHANNEL_CONSUMER))

#define SHARE_CHANNEL_CONSUMER_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_CHANNEL_CONSUMER, ShareChannelConsumerClass))

typedef struct _ShareChannelConsumerClass   ShareChannelConsumerClass;

struct _ShareChannelConsumer
{
    ShareObject object;
};

struct _ShareChannelConsumerClass
{
    ShareObjectClass parent_class;
};


GType 
share_channel_consumer_get_type       (void);

ShareChannelConsumer * 
share_channel_consumer_new            (void);

void 
share_channel_consumer_data_received  (ShareChannelConsumer *consumer,
                                       ShareData *data);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_CHANNEL_CONSUMER_H__ */
