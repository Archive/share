
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_AUTHENTICATION_INFO_H__
#define __SHARE_AUTHENTICATION_INFO_H__

#include <share/share_types.h>
#include <share/share_session.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_AUTHENTICATION_INFO \
            (share_authentication_info_get_type())

#define SHARE_AUTHENTICATION_INFO(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_AUTHENTICATION_INFO, ShareAuthenticationInfo))

#define SHARE_AUTHENTICATION_INFO_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_AUTHENTICATION_INFO, ShareAuthenticationInfoClass))

#define SHARE_IS_AUTHENTICATION_INFO(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_AUTHENTICATION_INFO))

#define SHARE_IS_AUTHENTICATION_INFO_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_AUTHENTICATION_INFO))

#define SHARE_AUTHENTICATION_INFO_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_AUTHENTICATION_INFO, ShareAuthenticationInfoClass))

typedef struct _ShareAuthenticationInfoClass ShareAuthenticationInfoClass;

struct _ShareAuthenticationInfo
{
    ShareObject object;
};

struct _ShareAuthenticationInfoClass
{
    ShareObjectClass parent_class;
};

/** The create ByteArray authentication action. */
static int SHARE_AUTHENTICATION_INFO_CREATE_BYTEARRAY   = 1 << 0;

/** The destroy ByteArray authentication action. */
static int SHARE_AUTHENTICATION_INFO_DESTROY_BYTEARRAY  = 1 << 1;

/** The create Channel authentication action. */
static int SHARE_AUTHENTICATION_INFO_CREATE_CHANNEL     = 1 << 2;

/** The destroy Channel authentication action. */
static int SHARE_AUTHENTICATION_INFO_DESTROY_CHANNEL    = 1 << 3;

/** The create Token authentication action. */
static int SHARE_AUTHENTICATION_INFO_CREATE_TOKEN       = 1 << 4;

/** The destroy Token authentication action. */
static int SHARE_AUTHENTICATION_INFO_DESTROY_TOKEN      = 1 << 5;

/** The join authentication action. */
static int SHARE_AUTHENTICATION_INFO_JOIN               = 1 << 6;

/** The create Session authentication action. */
static int SHARE_AUTHENTICATION_INFO_CREATE_SESSION     = 1 << 7;

/** The destroy Session authentication action. */
static int SHARE_AUTHENTICATION_INFO_DESTROY_SESSION    = 1 << 8;

/** The create Client authentication action. */
static int SHARE_AUTHENTICATION_INFO_CREATE_CLIENT      = 1 << 9;

/** The destroy Client authentication action. */
static int SHARE_AUTHENTICATION_INFO_DESTROY_CLIENT     = 1 << 10;

GType
share_authentication_info_get_type        (void);

ShareAuthenticationInfo * 
share_authentication_info_new             (ShareSession *session,
                                           gint action,
                                           gchar *name,
                                           gchar type);

gint 
share_authentication_info_get_action      (ShareAuthenticationInfo *info);

GObject * 
share_authentication_info_get_challenge   (ShareAuthenticationInfo *info);

gchar * 
share_authentication_info_get_name        (ShareAuthenticationInfo *info);

ShareSession * 
share_authentication_info_get_session     (ShareAuthenticationInfo *info);

gchar 
share_authentication_info_get_object_type (ShareAuthenticationInfo *info);

void 
share_authentication_info_set_challenge   (ShareAuthenticationInfo *info,
                                           GObject *challenge);

gchar * 
share_authentication_info_to_string       (ShareAuthenticationInfo *info);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_AUTHENTICATION_INFO_H__ */
