
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The ByteArray event. ByteArray events are created for the following actions:
 * <itemizedlist>
 *   <listitem>when a Client has joined a ByteArray.</listitem>
 *   <listitem>when a Client has left a ByteArray.</listitem>
 *   <listitem>when the value of a ByteArray changes.</listitem>
 *   <listitem>when a Client has been invited to join a ByteArray.</listitem>
 *   <listitem>when a Client has been expelled from a ByteArray.</listitem>
 * </itemizedlist>
 */

#include <share/share_byte_array_event.h>

static void share_byte_array_event_class_init     (ShareByteArrayEventClass *class);
static void share_byte_array_event_instance_init  (ShareByteArrayEvent *instance);


GType 
share_byte_array_event_get_type(void)
{
    printf("share_byte_array_event_get_type called.\n");
}


static void 
share_byte_array_event_class_init(ShareByteArrayEventClass *class)
{
    printf("share_byte_array_event_class_init called.\n");
}


static void 
share_byte_array_event_instance_init(ShareByteArrayEvent *instance)
{
    printf("share_byte_array_event_instance_init called.\n");
}


/**
 * share_byte_array_event_new:
 * @session: the session associated with this event.
 * @client_name: the name of the client.
 * @byte_array: the byte array associated with this event.
 * @type: the type of event.
 *
 * Constructor for the ByteArrayEvent object. A new byte array event is
 * generated for a client action associated with the given byte array
 * within the given session.
 *
 * Returns: a ShareByteArrayEvent object.
 */

ShareByteArrayEvent * 
share_byte_array_event_new(ShareSession *session,
                           gchar *client_name,
                           ShareByteArray *byte_array,
                           gint type)
{
    printf("share_byte_array_event_new called.\n");
}


/**
 * share_byte_array_event_get_byte_array:
 * @event: the ByteArray event.
 *
 * Get the ByteArray associated with this event.
 *
 * Returns: the ByteArray associated with this event.
 */

ShareByteArray * 
share_byte_array_event_get_byte_array(ShareByteArrayEvent *event)
{
    printf("share_byte_array_event_get_byte_array called.\n");
}


/**
 * share_byte_array_event_get_client_name:
 * @event: the ByteArray event.
 *
 * Get the name of the Client that generated this event.
 *
 * Returns: the name of the Client that generated this event.
 */

gchar * 
share_byte_array_event_get_client_name(ShareByteArrayEvent *event)
{
    printf("share_byte_array_event_get_client_name called.\n");
}


/**
 * share_byte_array_event_get_session:
 * @event: the ByteArray event.
 *
 * Get the Session associated with this event.
 *
 * Returns: the Session associated with this event.
 */

ShareSession * 
share_byte_array_event_get_session(ShareByteArrayEvent *event)
{
    printf("share_byte_array_event_get_session called.\n");
}


/**
 * share_byte_array_event_to_string:
 * @event: the ByteArray event.
 *
 * Return a short description of this ByteArray event.
 *
 * Returns: a string containing a description of this ByteArray event.
 */

gchar * 
share_byte_array_event_to_string(ShareByteArrayEvent *event)
{
    printf("share_byte_array_event_to_string called.\n");
}
