
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The ByteArray Manager object.
 */

#include <share/share_byte_array_manager.h>

static void share_byte_array_manager_class_init	    (ShareByteArrayManagerClass *class);
static void share_byte_array_manager_instance_init  (ShareByteArrayManager *instance);


GType 
share_byte_array_manager_get_type(void)
{
    printf("share_byte_array_manager_get_type called.\n");
}


static void 
share_byte_array_manager_class_init(ShareByteArrayManagerClass *class)
{
    printf("share_byte_array_manager_class_init called.\n");
}


static void 
share_byte_array_manager_instance_init(ShareByteArrayManager *instance)
{
    printf("share_byte_array_manager_instance_init called.\n");
}


ShareByteArrayManager * 
share_byte_array_manager_new(void)
{
    printf("share_byte_array_manager_new called.\n");
}


/**
 * share_byte_array_manager_byte_array_request:
 * @byte_array_manager: the ByteArray Manager.
 * @byte_array: the ByteArray the Client is interested in.
 * @info: the authentication information.
 * @client: the Client.
 *
 * Called when there is a Client interested in performing a priviledged
 * operation on a managed ByteArray.
 *
 * The following priviledged operations could occur:
 * <itemizedlist>
 *   <listitem>JOIN a managed ByteArray.</listitem>
 * </itemizedlist>
 *
* See: share_authentication_info
 *
 * Returns: true if the client is allowed to perform the priviledged
 * operation; false if not.
 */

gboolean 
share_byte_array_manager_byte_array_request(
                                     ShareByteArrayManager *byte_array_manager,
                                     ShareByteArray * byte_array,
                                     ShareAuthenticationInfo *info,
                                     ShareClient *client)
{
    printf("share_byte_array_manager_byte_array_request called.\n");
}
