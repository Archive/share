
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_TYPES_H__
#define __SHARE_TYPES_H__

#include <share/share_object.h>

typedef guchar byte;

typedef enum {
    SHARE_RESULT_OKAY = 0,
    SHARE_ALREADY_BOUND_EXCEPTION,
    SHARE_CLIENT_NOT_GRABBING_EXCEPTION,
    SHARE_CLIENT_NOT_RELEASED_EXCEPTION,
    SHARE_CONNECTION_EXCEPTION,
    SHARE_INVALID_CLIENT_EXCEPTION,
    SHARE_INVALID_URL_EXCEPTION,
    SHARE_SHARE_EXCEPTION,
    SHARE_MANAGER_EXISTS_EXCEPTION,
    SHARE_NAME_IN_USE_EXCEPTION,
    SHARE_NO_REGISTRY_EXCEPTION,
    SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION,
    SHARE_NO_SUCH_CHANNEL_EXCEPTION,
    SHARE_NO_SUCH_CLIENT_EXCEPTION,
    SHARE_NO_SUCH_CONSUMER_EXCEPTION,
    SHARE_NO_SUCH_HOST_EXCEPTION,
    SHARE_NO_SUCH_MANAGER_EXCEPTION,
    SHARE_NO_SUCH_SESSION_EXCEPTION,
    SHARE_NO_SUCH_TOKEN_EXCEPTION,
    SHARE_NOT_BOUND_EXCEPTION,
    SHARE_PERMISSION_DENIED_EXCEPTION,
    SHARE_PORT_IN_USE_EXCEPTION,
    SHARE_REGISTRY_EXISTS_EXCEPTION,
    SHARE_TIMED_OUT_EXCEPTION,
    SHARE_UNKNOWN_EXCEPTION
} ShareStatus;

/* Forward references to commonly used objects. */

typedef struct _ShareAuthenticationInfo   ShareAuthenticationInfo;
typedef struct _ShareByteArray            ShareByteArray;
typedef struct _ShareByteArrayEvent       ShareByteArrayEvent;
typedef struct _ShareByteArrayManager     ShareByteArrayManager;
typedef struct _ShareChannel              ShareChannel;
typedef struct _ShareChannelConsumer      ShareChannelConsumer;
typedef struct _ShareClient               ShareClient;
typedef struct _ShareData                 ShareData;
typedef struct _ShareSession              ShareSession;
typedef struct _ShareToken                ShareToken;
typedef struct _ShareTokenEvent           ShareTokenEvent;

#endif /* __SHARE_TYPES_H__ */
