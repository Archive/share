
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Share Authentication Information object.
 *
 * This object contains all the information needed by a Client to determine
 * what they are being asked to authenticate.
 *
 * If a Share Manageable object (ByteArray, Channel, Session or Token)
 * has a Manager attached to it, any Client that tries to join that object
 * will be authenticated, and asked to provide a response. If a Client is
 * joined to a managed Session, and wishes to create or destroy a ByteArray,
 * Channel or Token, the same authentication process takes place.
 */

#include <share/share_authentication_info.h>

static void share_authentication_info_class_init     (ShareAuthenticationInfoClass *class);
static void share_authentication_info_instance_init  (ShareAuthenticationInfo *instance);


GType 
share_authentication_info_get_type(void)
{
    static GType authentication_info_type = 0;

    if (!authentication_info_type) {
        static const GTypeInfo authentication_info_info = {
            sizeof(ShareAuthenticationInfoClass),
            NULL,			/* base_init */
            NULL,			/* base_finalize */
            (GClassInitFunc) share_authentication_info_class_init,
            NULL,			/* class_finalize */
            NULL,			/* class_data */
            sizeof(ShareAuthenticationInfo),
            0,				/* n_preallocs */
            (GInstanceInitFunc) share_authentication_info_instance_init,
        };

        authentication_info_type = g_type_register_static(SHARE_TYPE_OBJECT,
                                                 "ShareAuthenticationInfo",
                                                 &authentication_info_info, 0);
    }

    return(authentication_info_type);
}


static void 
share_authentication_info_class_init(ShareAuthenticationInfoClass *class)
{
    printf("share_authentication_info_class_init called.\n");
}


static void 
share_authentication_info_instance_init(ShareAuthenticationInfo *instance)
{
    printf("share_authentication_info_instance_init called.\n");
}


/**
 * share_authentication_info_new:
 * @session: the Session associated with this authentication operation.
 * @action: the authentication action.
 * @name: the name of the manageable object.
 * @type: the type of the manageable object.
 *
 * The constructor for the AuthenticationInfo object. A new authentication
 * info object is generated every time a Client tries to perform a
 * priviledged action on a managed object.
 *
 * Returns: a ShareAuthenticationInfo object.
 */

ShareAuthenticationInfo * 
share_authentication_info_new(ShareSession *session,
                              gint action,
                              gchar *name,
                              gchar type)
{
    printf("share_authentication_info_new called.\n");
}


/**
 * share_authentication_info_get_action:
 * @info: the authentication info object.
 *
 * Get the authentication action (CREATE, DESTROY or JOIN).
 *
 * Returns: the authentication action.
 */

gint 
share_authentication_info_get_action(ShareAuthenticationInfo *info)
{
    printf("share_authentication_info_get_action called.\n");
}


/**
 * share_authentication_info_get_challenge:
 * @info: the authentication info object.
 *
 * Get the challenge given by the manager.
 *
 * Returns: the challenge given by the manager.
 */

GObject * 
share_authentication_info_get_challenge(ShareAuthenticationInfo *info)
{
    printf("share_authentication_info_get_challenge called.\n");
}


/**
 * share_authentication_info_get_name:
 * @info: the authentication info object.
 *
 * Get the name of the object associated with this authentication operation.
 * This will be the name of the ByteArray, Channel or Token being created,
 * or the name of the ByteArray, Channel, Session or Token being destroyed,
 * or the name of the manageable object the Client is trying to join.
 *
 * Returns: the name of the object associated with this authentication 
 *          operation.
 */

gchar * 
share_authentication_info_get_name(ShareAuthenticationInfo *info)
{
    printf("share_authentication_info_get_name called.\n");
}


/**
 * share_authentication_info_get_session:
 * @info: the authentication info object.
 *
 * Get the Session associated with this authentication operation.
 *
 * Returns: the Session associated with this authentication operation.
 */

ShareSession * 
share_authentication_info_get_session(ShareAuthenticationInfo *info)
{
    printf("share_authentication_info_get_session called.\n");
}


/**
 * share_authentication_info_get_object_type:
 * @info: the authentication info object.
 *
 * Get the type of this manageable object (ByteArray, Channel, Session
 * or Token).
 *
 * Returns: the type of this manageable object.
 */

gchar 
share_authentication_info_get_object_type(ShareAuthenticationInfo *info)
{
    printf("share_authentication_info_get_object_type called.\n");
}


/**
 * share_authentication_info_set_challenge:
 * @info: the authentication info object.
 * @challenge: the challenge.
 *
 * Set the challenge for this authentication.  This is an operation that
 * is performed by the manager just before it passes the authentication
 * information onto the client
 */

void 
share_authentication_info_set_challenge(ShareAuthenticationInfo *info,
                                        GObject *challenge)
{
    printf("share_authentication_info_set_challenge called.\n");
}

/**
 * share_authentication_info_to_string:
 * @info: the authentication info object.
 *
 * Print a short description of this AuthenticationInfo object.
 *
 * Returns: a string containing a description of this AuthenticationInfo object.
 */

gchar * 
share_authentication_info_to_string(ShareAuthenticationInfo *info)
{
    printf("share_authentication_info_to_string called.\n");
}
