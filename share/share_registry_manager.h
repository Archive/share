
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_REGISTRY_MANAGER_H__
#define __SHARE_REGISTRY_MANAGER_H__

#include <share/share_manager.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_REGISTRY_MANAGER \
            (share_registry_manager_get_type())

#define SHARE_REGISTRY_MANAGER(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_REGISTRY_MANAGER, ShareRegistryManager))

#define SHARE_REGISTRY_MANAGER_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_REGISTRY_MANAGER, ShareRegistryManagerClass))

#define SHARE_IS_REGISTRY_MANAGER(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_REGISTRY_MANAGER))

#define SHARE_IS_REGISTRY_MANAGER_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_REGISTRY_MANAGER))

#define SHARE_REGISTRY_MANAGER_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_REGISTRY_MANAGER, ShareRegistryManagerClass))

typedef struct _ShareRegistryManager        ShareRegistryManager;
typedef struct _ShareRegistryManagerClass   ShareRegistryManagerClass;

struct _ShareRegistryManager
{
    ShareManager manager;
};

struct _ShareRegistryManagerClass
{
    ShareManagerClass parent_class;
};


GType 
share_registry_manager_get_type         (void);

ShareRegistryManager * 
share_registry_manager_new              (void);

gboolean 
share_registry_manager_registry_request (ShareRegistryManager *registry_manager,
                                         ShareAuthenticationInfo *info,
                                         ShareClient *client);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_REGISTRY_MANAGER_H__ */
