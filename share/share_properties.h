
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_PROPERTIES_H__
#define __SHARE_PROPERTIES_H__

#include <share/share_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_PROPERTIES \
            (share_properties_get_type())

#define SHARE_PROPERTIES(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_PROPERTIES, ShareProperties))

#define SHARE_PROPERTIES_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_PROPERTIES, SharePropertiesClass))

#define SHARE_IS_PROPERTIES(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_PROPERTIES))

#define SHARE_IS_PROPERTIES_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_PROPERTIES))

#define SHARE_PROPERTIES_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_PROPERTIES, SharePropertiesClass))

typedef struct _ShareProperties        ShareProperties;
typedef struct _SharePropertiesClass   SharePropertiesClass;

struct _ShareProperties
{
    ShareObject object;
};

struct _SharePropertiesClass
{
    ShareObjectClass parent_class;
};


GType  
share_properties_get_type   (void);

ShareProperties * 
share_properties_new        (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_PROPERTIES_H__ */
