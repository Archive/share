
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_SESSION_EVENT_H__
#define __SHARE_SESSION_EVENT_H__

#include <share/share_event.h>
#include <share/share_url_string.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_SESSION_EVENT \
            (share_session_event_get_type())

#define SHARE_SESSION_EVENT(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_SESSION_EVENT, ShareSessionEvent))

#define SHARE_SESSION_EVENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_SESSION_EVENT, ShareSessionEventClass))

#define SHARE_IS_SESSION_EVENT(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_SESSION_EVENT))

#define SHARE_IS_SESSION_EVENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_SESSION_EVENT))

#define SHARE_SESSION_EVENT_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_SESSION_EVENT, ShareSessionEventClass))

typedef struct _ShareSessionEvent        ShareSessionEvent;
typedef struct _ShareSessionEventClass   ShareSessionEventClass;

struct _ShareSessionEvent
{
    ShareEvent event;
};

struct _ShareSessionEventClass
{
    ShareEventClass parent_class;
};


/** The Session ByteArray created event type. */
static int SHARE_SESSION_BYTEARRAY_CREATED   = 1 << 0;

/** The Session ByteArray destroyed event type. */
static int SHARE_SESSION_BYTEARRAY_DESTROYED = 1 << 1;

/** The Session Channel created event type. */
static int SHARE_SESSION_CHANNEL_CREATED     = 1 << 2;

/** The Session Channel destroyed event type. */
static int SHARE_SESSION_CHANNEL_DESTROYED   = 1 << 3;

/** The Session Token created event type. */
static int SHARE_SESSION_TOKEN_CREATED       = 1 << 4;

/** The Session Token destroyed event type. */
static int SHARE_SESSION_TOKEN_DESTROYED     = 1 << 5;

/** The Session joined event type. */
static int SHARE_SESSION_JOINED              = 1 << 6;

/** The Session left event type. */
static int SHARE_SESSION_LEFT                = 1 << 7;

/** The Session invited event type. */
static int SHARE_SESSION_INVITED             = 1 << 8;

/** The Session expelled event type. */
static int SHARE_SESSION_EXPELLED            = 1 << 9;

/** The Session destroyed event type. */
static int SHARE_SESSION_DESTROYED           = 1 << 10;

GType 
share_session_event_get_type             (void);

ShareSessionEvent * 
share_session_event_new                  (ShareSession *session,
                                          gchar *client_name,
                                          ShareUrlString *resource_name,
                                          gint type);

gchar * 
share_session_event_get_client_name      (ShareSessionEvent *event);

gchar * 
share_session_event_get_resource_name    (ShareSessionEvent *event);

ShareSession * 
share_session_event_get_session          (ShareSessionEvent *event);

gchar * 
share_session_event_to_string            (ShareSessionEvent *event);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_SESSION_EVENT_H__ */
