
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The SessionFactory object.
 *
 * This is a factory object that is the basis for creating new sessions.
 * These would be of the appropriate type. The session would then be 
 * published by the naming service, and tied to a specific URL.
 */

#include <share/share_session_factory.h>

static void share_session_factory_class_init     (ShareSessionFactoryClass *class);
static void share_session_factory_instance_init  (ShareSessionFactory *instance);


GType    
share_session_factory_get_type(void)
{
    printf("share_session_factory_get_type called.\n");
}


static void 
share_session_factory_class_init(ShareSessionFactoryClass *class)
{
    printf("share_session_factory_class_init called.\n");
}


static void 
share_session_factory_instance_init(ShareSessionFactory *instance)
{
    printf("share_session_factory_instance_init called.\n");
}


/**
 * share_session_factory_destroy_session:
 * @session_factory: the Session Factory.
 * @client: the Client wishing to destroy this Session..
 * @url_string: the URLString for this session.
 *
 * Destroy the Session referenced by the given URL string.
 *
 * An indication is delivered to each listener for the Registry where
 * this Session is stored, that it has been destroyed. If the Registry
 * where this Session is stored is managed, then the Client is authenticated
 * to determine if it is permitted to do this operation.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid is some
 *	 way (ie. share_client_get_name returns null, or returns a name
 *	 that is not the same as the object name portion of the
 *	 ShareURLString parameter).</listitem>
 *   <listitem>SHARE_INVALID_URL_EXCEPTION if the url string given is invalid.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if no Registry process running.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if the host name in the url string
 *	 doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if a Client of this type doesn't 
 *       exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if a session of this type could
 *	 not be returned.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *	 permission for this operation.</listitem>
 *   <listitem>SHARE_NOT_BOUND_EXCEPTION if no object bound to this url string.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_factory_destroy_session(ShareSessionFactory *session_factory,
                                      ShareClient *client,
                                      ShareUrlString *url_string)
{
    printf("share_session_factory_destroy_session called.\n");
}


/**
 * share_session_factory_session_exists:
 * @session_factory: the Session Factory.
 * @url_string: the Session URLString to check.
 * @exists: return true if the Session already exists; false if it 
 *        doesn't.
 *
 * Check if a Session with the given url string, already exists.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if the host name in the url string
 *       doesn't exist.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if no Registry process running.</listitem>
 *   <listitem>SHARE_INVALID_URL_EXCEPTION if the url string given is invalid.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_factory_session_exists(ShareSessionFactory *session_factory,
                                     ShareUrlString *url_string,
                                     gboolean *exists)
{
    printf("share_session_factory_session_exists called.\n");
}


/**
 * share_session_factory_session_managed:
 * @session_factory: the Session Factory.
 * @url_string: the Session URLString to check.
 * @managed: return true if the Session is managed; false if it isn't.
 *
 * Check if the Session with the given url is managed.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if the host name in the url string
 *       doesn't exist.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if no Registry process running.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if the session name in the url 
 *       string doesn't exist.</listitem>
 *   <listitem>SHARE_INVALID_URL_EXCEPTION if the url string given is invalid.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_factory_session_managed(ShareSessionFactory *session_factory,
                                      ShareUrlString *url_string,
                                      gboolean *managed)
{
    printf("share_session_factory_session_managed called.\n");
}


/**
 * share_session_factory_list_sessions:
 * @session_factory: the Session Factory.
 * @names: return an array of strings of the names of all the known 
 *        bound Share Sessions, or a zero length array if there are no 
 *        bound objects.
 *
 * Lists all the url strings of the known bound Share Sessions.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if no Registry process running.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *	 in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_factory_list_sessions(ShareSessionFactory *session_factory,
                                    ShareUrlString *names[])
{
    printf("share_session_factory_list_sessions called.\n");
}


/**
 * share_session_factory_list_sessions_on_host:
 * @session_factory: the Session Factory.
 * @host: host name of the machine to look for bound Share sessions on.
 * @connection_type: the type of connection.
 * @names: return an array of strings of the names of all the known 
 *        bound Share Sessions on the given host, or null if there are no 
 *        bound objects.
 *
 * Lists all the url strings of the known bound Share Sessions on the given
 * host with the given connection type.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if the host doesn't exist.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if no Registry process running.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_factory_list_sessions_on_host(
                                     ShareSessionFactory *session_factory,
                                     gchar * *host,
                                     gchar * *connection_type,
                                     ShareUrlString *names[])
{
    printf("share_session_factory_list_sessions_on_host called.\n");
}
