
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The Byte Array object. A ByteArray contains an array of bytes that
 * is shared between all Clients that are currently joined to it.
 */

#include <share/share_byte_array.h>
#include <share/share_marshal.h>

enum {
    SHARE_BYTE_ARRAY_JOINED_SIGNAL,
    SHARE_BYTE_ARRAY_LEFT_SIGNAL,
    SHARE_BYTE_ARRAY_VALUE_CHANGED_SIGNAL,
    SHARE_BYTE_ARRAY_INVITED_SIGNAL,
    SHARE_BYTE_ARRAY_EXPELLED_SIGNAL,
    SHARE_BYTE_ARRAY_LAST_SIGNAL
};

static void share_byte_array_class_init     (ShareByteArrayClass *class);
static void share_byte_array_instance_init  (ShareByteArray      *instance);


static guint byte_array_signals[SHARE_BYTE_ARRAY_LAST_SIGNAL] = { 0 };
static ShareManageableClass *parent_class = NULL;


GType
share_byte_array_get_type(void)
{
    static GType byte_array_type = 0;

    if (!byte_array_type) {
        static const GTypeInfo byte_array_info = {
            sizeof(ShareByteArrayClass),
            NULL,			/* base_init */
            NULL,			/* base_finalize */
            (GClassInitFunc) share_byte_array_class_init,
            NULL,			/* class_finalize */
            NULL,			/* class_data */
            sizeof(ShareByteArray),
            0,				/* n_preallocs */
            (GInstanceInitFunc) share_byte_array_instance_init,
        };

        byte_array_type = g_type_register_static(SHARE_TYPE_MANAGEABLE,
                                                 "ShareByteArray",
                                                 &byte_array_info, 0);
    }

    return(byte_array_type);
}


static void 
share_byte_array_class_init(ShareByteArrayClass *class)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS(class);

    parent_class = g_type_class_peek_parent(class);

    byte_array_signals[SHARE_BYTE_ARRAY_JOINED_SIGNAL] = 
        g_signal_new("share_byte_array_joined",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareByteArrayClass,
                            share_byte_array_joined),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_BYTE_ARRAY,
            SHARE_TYPE_BYTE_ARRAY_EVENT);

    byte_array_signals[SHARE_BYTE_ARRAY_LEFT_SIGNAL] = 
        g_signal_new("share_byte_array_left",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareByteArrayClass, 
                            share_byte_array_left),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_BYTE_ARRAY,
            SHARE_TYPE_BYTE_ARRAY_EVENT);

    byte_array_signals[SHARE_BYTE_ARRAY_VALUE_CHANGED_SIGNAL] = 
        g_signal_new("share_byte_array_value_changed",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareByteArrayClass, 
                            share_byte_array_value_changed),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_BYTE_ARRAY,
            SHARE_TYPE_BYTE_ARRAY_EVENT);

    byte_array_signals[SHARE_BYTE_ARRAY_INVITED_SIGNAL] = 
        g_signal_new("share_byte_array_invited",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareByteArrayClass, 
                            share_byte_array_invited),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_BYTE_ARRAY,
            SHARE_TYPE_BYTE_ARRAY_EVENT);

    byte_array_signals[SHARE_BYTE_ARRAY_EXPELLED_SIGNAL] = 
        g_signal_new("share_byte_array_expelled",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareByteArrayClass,
                            share_byte_array_expelled),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_BYTE_ARRAY,
            SHARE_TYPE_BYTE_ARRAY_EVENT);
}


static void 
share_byte_array_instance_init(ShareByteArray *instance)
{
    printf("share_byte_array_instance_init called.\n");
}


ShareByteArray * 
share_byte_array_new(void)
{
    ShareByteArray *byte_array;

    byte_array = g_object_new(share_byte_array_get_type(), NULL);

    return(byte_array);
}


/**
 * share_byte_array_get_value_as_bytes:
 * @byte_array: the shared ByteArray.
 * @value: the value of the shared ByteArray returned as an array of
 *        bytes.
 *
 * Get the current value for this shared ByteArray, as an array of bytes.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this shared ByteArray 
 *       doesn't exist.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_byte_array_get_value_as_bytes(ShareByteArray *byte_array,
                                    byte *value[])
{
    printf("share_byte_array_get_value_as_bytes called.\n");
}


/**
 * share_byte_array_get_value_as_object:
 * @byte_array: the shared ByteArray.
 * @object: the value of the shared ByteArray returned as a GObject.
 *
 * Get the current value for this shared ByteArray as a GObject.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this shared ByteArray 
 *       doesn't exist.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_byte_array_get_value_as_object(ShareByteArray *byte_array,
                                     GObject *object)
{
    printf("share_byte_array_get_value_as_object called.\n");
}

 
/**
 * share_byte_array_get_value_as_string:
 * @byte_array: the shared ByteArray.
 * @string: the value of the shared ByteArray returned as a string.
 *
 * Get the current value for this shared ByteArray, as a string.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this shared ByteArray 
 *       doesn't exist.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_byte_array_get_value_as_string(ShareByteArray *byte_array,
                                     gchar *string)
{
    printf("share_byte_array_get_value_as_string called.\n");
}

 
/**
 * share_byte_array_set_value:
 * @byte_array: the shared ByteArray.
 * @client: the Client wishing to set the value of this ByteArray.
 * @value: the new byte array value for this shared ByteArray.
 *
 * Sets a new value for this shared ByteArray using the given byte array value.
 * The new value is sent to all other instances of this shared ByteArray.
 * [XXX: All ByteArray listeners will have their byteArrayValueChanged 
 *       method invoked].
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this shared ByteArray 
 *       doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client does not have 
 *       permission to change the value of this ByteArray.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this 
 *       operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_byte_array_set_value(ShareByteArray *byte_array,
                           ShareClient *client,
                           byte *value[])
{
    printf("share_byte_array_set_value called.\n");
}

 
/**
 * share_byte_array_set_value_with_offset_and_length:
 * @byte_array: the shared ByteArray.
 * @client: the Client wishing to set the value of this ByteArray.
 * @value: the byte array that is the source of the new shared
 *        ByteArray value.
 * @offset: the initial offset within the byte array.
 * @length: the number of bytes to use.
 *
 * Sets a new value for this shared ByteArray using a subset of the given
 * ByteArray. The new value is sent to all other instances of this shared
 * ByteArray.
 * [XXX: All ByteArray listeners will have their byteArrayValueChanged 
 *       method invoked].
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this shared ByteArray
 *	 doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client does not have
 *	 permission to change the value of this ByteArray.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_byte_array_set_value_with_offset_and_length(ShareByteArray *byte_array,
                                                  ShareClient *client,
                                                  byte *value[],
                                                  gint offset,
                                                  gint length)
{
    printf("share_byte_array_set_value_with_offset_and_length called.\n");
}

 
/**
 * share_byte_array_set_value_from_string:
 * @byte_array: the shared ByteArray.
 * @client: the Client wishing to set the value of this ByteArray.
 * @string: the string value from which an array of bytes is set.
 *
 * Sets a new value for this shared ByteArray using the given string.
 * The new value is sent to all other instances of this shared ByteArray.
 * [XXX: All ByteArray listeners will have their byteArrayValueChanged 
 *       method invoked].
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this shared ByteArray
 *	 doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client does not have
 *	 permission to change the value of this ByteArray.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_byte_array_set_value_from_string(ShareByteArray *byte_array,
                                       ShareClient *client,
                                       gchar *string)
{
    printf("share_byte_array_set_value_from_string called.\n");
}

 
/**
 * share_byte_array_set_value_from_object:
 * @byte_array: the shared ByteArray.
 * @client: the Client wishing to set the value of this ByteArray.
 * @object: the GObject value which is serialized into an array of bytes.
 *
 * Sets a new value for this shared ByteArray using the given GObject.
 * The new value is sent to all other instances of this shared ByteArray.
 * [XXX: All ByteArray listeners will have their byteArrayValueChanged 
 *       method invoked].
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this shared ByteArray
 *       doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client does not have
 *       permission to change the value of this ByteArray.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *       operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_byte_array_set_value_from_object(ShareByteArray *byte_array,
                                       ShareClient *client,
                                       GObject *object)
{
    printf("share_byte_array_set_value_from_object called.\n");
}
