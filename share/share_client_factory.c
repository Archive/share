
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The ClientFactory object.
 *
 * This is a factory object that is used to create a special Client that
 * can be invited to join a Session, ByteArray, Channel or Token. If you
 * do not need this functionality, then you should just implement the
 * Client interface.
 *
 * See the Share User Guide for examples of using both a normal Client and
 * the ClientFactory.
 */

#include <share/share_client_factory.h>

static void share_client_factory_class_init     (ShareClientFactoryClass *class);
static void share_client_factory_instance_init  (ShareClientFactory *instance);


GType 
share_client_factory_get_type(void)
{
    printf("share_client_factory_get_type called.\n");
}


static void 
share_client_factory_class_init(ShareClientFactoryClass *class)
{
    printf("share_client_factory_class_init called.\n");
}


static void 
share_client_factory_instance_init(ShareClientFactory *instance)
{
    printf("share_client_factory_instance_init called.\n");
}


ShareClientFactory * 
share_client_factory_new(void)
{
    printf("share_client_factory_new called.\n");
}


/**
 * share_client_factory_create_client:
 * @client_factory: the Client factory.
 * @client: the Client to use for authentication purposes.
 * This client will also be used for authentication purposes if the
 * Registry running on the server machine where this Client URL will be
 * stored, is managed.
 * @url_string: the URLString for this client.
 *
 * Creates a new special Client of the appropriate type. This Client will be
 * capable of being invited to join a Share Session. If a Client with this
 * URL already exists in the registry, then a status of
 * SHARE_ALREADY_BOUND_EXCEPTION will be returned.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_ALREADY_BOUND_EXCEPTION if this url string is already bound 
 *       to an object.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid is some 
 *       way (ie. share_client_get_name returns null, or returns a name 
 *       that is not the same as the object name portion of the 
 *       ShareURLString parameter).</listitem>
 *   <listitem>SHARE_INVALID_URL_EXCEPTION if the url string given is invalid.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if no Registry process running.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if the host name in the url string 
 *       doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if a Client of this type doesn't 
 *       exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *       permission for this operation.</listitem>
 *   <listitem>SHARE_PORT_IN_USE_EXCEPTION if this port is being used by another
 *       application.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this 
 *       operation in the given timeout period.</listitem>
 * </itemizedlist>
 *
 * See: share_client
 */

ShareStatus 
share_client_factory_create_client(ShareClientFactory *client_factory,
                                   ShareClient *client,
                                   ShareUrlString *url_string)
{
    printf("share_client_factory_create_client called.\n");
}


/**
 * share_client_factory_lookup_client:
 * @client_factory: the Client factory.
 * @url_string: the ShareURLString for this client.
 * @client: a reference to the Client associated with the given URL 
 *        string.
 *
 * Return a reference to the special Client bound by the given URL String.
 * This Client can then be invited to to join a Share Session.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_URL_EXCEPTION if the url string given is invalid.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if no Registry process running.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if the host name in the url string
 *	 doesn't exist.</listitem>
 *   <listitem>SHARE_NOT_BOUND_EXCEPTION if no object bound to this url string.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_client_factory_lookup_client(ShareClientFactory *client_factory,
                                   ShareUrlString *url_string,
                                   ShareClient *client)
{
    printf("share_client_factory_lookup_client called.\n");
}


/**
 * share_client_factory_destroy_client:
 * @client_factory: the Client factory.
 * @client: the Client wishing to destroy this special Client.
 * @url_string: the ShareURLString for this Client.
 *
 * Destroy the Client referenced by the given URL string.
 *
 * An indication is delivered to each listener for the Registry where
 * this Client is stored, that it has been destroyed. If the Registry
 * where this Client is stored is managed, then the Client parameter is
 * used for authentication purposes to determine if this operation is
 * permitted.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid is some
 *	 way (ie. share_client_get_name returns null, or returns a name
 *	 that is not the same as the object name portion of the
 *	 ShareURLString parameter).</listitem>
 *   <listitem>SHARE_INVALID_URL_EXCEPTION if the url string given is invalid.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if no Registry process running.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if the host name in the url string
 *	 doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if a Client of this type doesn't 
 *       exist.</listitem>
 *   <listitem>SHARE_NOT_BOUND_EXCEPTION if no object bound to this url string.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *	 permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_client_factory_destroy_client(ShareClientFactory *client_factory,
                                    ShareClient *client,
                                    ShareUrlString *url_string)
{
    printf("share_client_factory_destroy_client called.\n");
}


/**
 * share_client_factory_client_exists:
 * @client_factory: the Client factory.
 * @url_string: the Client ShareURLString to check.
 * @exists: set true if the Client already exists; false if it doesn't.
 *
 * Check if a Client with the given url string already exists.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_URL_EXCEPTION if the url string given is invalid.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if no Registry process running.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if the host name in the url string
 *	 doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_client_factory_client_exists(ShareClientFactory *client_factory,
                                   ShareUrlString *url_string,
                                   gboolean *exists)
{
    printf("share_client_factory_client_exists called.\n");
}


/**
 * share_client_factory_list_clients:
 * @client_factory: the Client factory.
 * @clients: an array of Strings of the names of all the known 
 * bound Share Clients, or a zero length array if there are no bound objects.
 *
 * Lists all the url strings of the known bound Share Clients.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if no Registry process running.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *       operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_client_factory_list_clients(ShareClientFactory *client_factory,
                                  ShareUrlString *clients[])
{
    printf("share_client_factory_list_clients called.\n");
}


/**
 * share_client_factory_list_clients_on_host:
 * @client_factory: the Client factory.
 * @host: host name of the machine to look for bound Share clients on.
 * @connection_type: the type of connection 
 * @clients: an array of strings of the names of all the known bound 
 *        Share Clients on the given host, or null if there are no bound 
 *        objects.
 *
 * Lists all the url strings of the known bound Share Clients on the given
 * host with the given connection type.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if the host doesn't exist.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if no Registry process running.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_client_factory_list_clients_on_host(ShareClientFactory *client_factory,
                                          gchar *host,
                                          gchar *connection_type,
                                          ShareUrlString *clients[])
{
    printf("share_client_factory_list_clients_on_host called.\n");
}
