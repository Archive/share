
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The Session object.
 */

#include <share/share_session.h>
#include <share/share_marshal.h>

enum {
    SHARE_BYTE_ARRAY_CREATED_SIGNAL,
    SHARE_BYTE_ARRAY_DESTROYED_SIGNAL,
    SHARE_CHANNEL_CREATED_SIGNAL,
    SHARE_CHANNEL_DESTROYED_SIGNAL,
    SHARE_SESSION_DESTROYED_SIGNAL,
    SHARE_SESSION_EXPELLED_SIGNAL,
    SHARE_SESSION_INVITED_SIGNAL,
    SHARE_SESSION_JOINED_SIGNAL,
    SHARE_SESSION_LEFT_SIGNAL,
    SHARE_TOKEN_CREATED_SIGNAL,
    SHARE_TOKEN_DESTROYED_SIGNAL,
    SHARE_SESSION_LAST_SIGNAL
};

static void share_session_class_init	 (ShareSessionClass *class);
static void share_session_instance_init  (ShareSession      *instance);


static guint session_signals[SHARE_SESSION_LAST_SIGNAL] = { 0 };
static ShareManageableClass *parent_class = NULL;


GType    
share_session_get_type(void)
{
    static GType session_type = 0;

    if (!session_type) {
	static const GTypeInfo session_info = {
            sizeof(ShareByteArrayClass),
            NULL,			/* base_init */
            NULL,			/* base_finalize */
            (GClassInitFunc) share_session_class_init,
            NULL,			/* class_finalize */
            NULL,			/* class_data */
            sizeof(ShareSession),
            0,				/* n_preallocs */
            (GInstanceInitFunc) share_session_instance_init,
	};

	session_type = g_type_register_static(SHARE_TYPE_MANAGEABLE,
                                              "ShareSession",
                                              &session_info, 0);
    }

    return(session_type);
}


static void 
share_session_class_init(ShareSessionClass *class)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS(class);

    parent_class = g_type_class_peek_parent(class);

    session_signals[SHARE_BYTE_ARRAY_CREATED_SIGNAL] =
	g_signal_new("share_byte_array_created",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareSessionClass,
                            share_byte_array_created),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_SESSION,
            SHARE_TYPE_SESSION_EVENT);

    session_signals[SHARE_BYTE_ARRAY_DESTROYED_SIGNAL] =
		g_signal_new("share_byte_array_destroyed",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareSessionClass,
                            share_byte_array_destroyed),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_SESSION,
            SHARE_TYPE_SESSION_EVENT);

    session_signals[SHARE_CHANNEL_CREATED_SIGNAL] =
		g_signal_new("share_channel_created",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareSessionClass,
                            share_channel_created),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_SESSION,
            SHARE_TYPE_SESSION_EVENT);

    session_signals[SHARE_CHANNEL_DESTROYED_SIGNAL] =
		g_signal_new("share_channel_destroyed",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareSessionClass,
                            share_channel_destroyed),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_SESSION,
            SHARE_TYPE_SESSION_EVENT);

    session_signals[SHARE_SESSION_DESTROYED_SIGNAL] =
		g_signal_new("share_session_destroyed",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareSessionClass,
                            share_session_destroyed),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_SESSION,
            SHARE_TYPE_SESSION_EVENT);

    session_signals[SHARE_SESSION_EXPELLED_SIGNAL] =
		g_signal_new("share_session_expelled",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareSessionClass,
                            share_session_expelled),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_SESSION,
            SHARE_TYPE_SESSION_EVENT);

    session_signals[SHARE_SESSION_INVITED_SIGNAL] =
		g_signal_new("share_session_invited",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareSessionClass,
                            share_session_invited),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_SESSION,
            SHARE_TYPE_SESSION_EVENT);

    session_signals[SHARE_SESSION_JOINED_SIGNAL] =
		g_signal_new("share_session_joined",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareSessionClass,
                            share_session_joined),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_SESSION,
            SHARE_TYPE_SESSION_EVENT);

    session_signals[SHARE_SESSION_LEFT_SIGNAL] =
		g_signal_new("share_session_left",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareSessionClass,
                            share_session_left),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_SESSION,
            SHARE_TYPE_SESSION_EVENT);

    session_signals[SHARE_TOKEN_CREATED_SIGNAL] =
		g_signal_new("share_token_created",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareSessionClass,
                            share_token_created),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_SESSION,
            SHARE_TYPE_SESSION_EVENT);

    session_signals[SHARE_TOKEN_DESTROYED_SIGNAL] =
		g_signal_new("share_token_destroyed",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareSessionClass,
                            share_token_destroyed),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_SESSION,
            SHARE_TYPE_SESSION_EVENT);
}


static void 
share_session_instance_init(ShareSession*instance)
{
    printf("share_session_instance_init called.\n");
}


/**
 * share_session_new_session:
 * @client: a client that will potentially be joined to this session.
 *	  This client will also be used for authentication purposes if the
 *	  Registry running on the server machine where this Session URL will
 *	  be stored, is managed.
 * @url_string: the URLString for this session.
 * @auto_join: if true, automatically join the session when it's created.
 *
 * Create a new session. If the Session already exists, a handle is
 * returned to that Session.
 *
 * Returns: a new #ShareSession.
 */

ShareSession *
share_session_factory_new_session(ShareClient *client,
                                  ShareUrlString *url_string,
                                  gboolean auto_join)
{
    printf("share_session_factory_new_session called.\n");
}


/**
 * share_session_new_session_with_manager:
 * @client: a client that will potentially be joined to this session.
 *	  This client will also be used for authentication purposes if the
 *	  Registry running on the server machine where this Session URL
 *	  will be stored, is managed.
 * @url_string: the URLString for this session.
 * @auto_join: if true, automatically join the session when it's created.
 * @session_manager: the session manager to associate with this session.
 *
 * Create a new session, and associate a session manager with that session.
 * If the Session already exists, a handle is returned to that Session.
 *
 * Returns: a new #ShareSession.
 */

ShareSession *
share_session_factory_new_session_with_manager(ShareClient *client,
                                               ShareUrlString *url_string,
                                               gboolean auto_join,
                                               ShareSessionManager *manager)
{
    printf("share_session_factory_new_session_with_manager called.\n");
}


/**
 * share_session_create_byte_array:
 * @session: the Session.
 * @client: a Client that will be used for authentication purposes if 
 *        this is a managed Session.
 * @byte_array_name: the name to give this ByteArray.
 * @auto_join: if true, automatically join the ByteArray when it's created.
 * @byte_array: the newly created ByteArray or a local reference to 
 *        it, if it was already created.
 *
 * Creates a shared ByteArray with the given name which can then
 * be used with the various ByteArray operations. This ByteArray is then
 * known to the Session. No ByteArrayManager is associated with this
 * ByteArray; any Client which has already joined the Session may freely
 * join this ByteArray.
 *
 * If a ByteArray with this name already exists, a reference to that
 * ByteArray is returned. If the ByteArray didn't already exist, then it's
 * initial value will be a zero filled byte array, one byte long.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NAME_IN_USE_EXCEPTION if a Client with this name is already
 *       joined to this ByteArray.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if the remote host associated with this
 *       Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *       permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_create_byte_array(ShareSession *session,
                                ShareClient *client,
                                gchar *byte_array_name,
                                gboolean auto_join,
                                ShareByteArray *byte_array)
{
    printf("share_session_create_byte_array called.\n");
}


/**
 * share_session_create_byte_array_with_manager:
 * @session: the Session.
 * @client: a Client that will be used for authentication purposes if 
 *        this is a managed Session.
 * @byte_array_name: the name to give this ByteArray.
 * @auto_join: if true, automatically join the ByteArray when it's created.
 * @manager: the manager of this ByteArray.
 * @byte_array: the newly created managed ByteArray.
 *
 * Creates a shared ByteArray with the given name which can then be used with
 * the various ByteArray operations. This ByteArray is then known to the
 * Session. A ByteArrayManager is associated with this ByteArray; Clients are
 * authenticated before they are allowed to join the ByteArray.
 *
 * The initial value for this newly created ByteArray will be a zero
 * filled byte array, one byte long.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_MANAGER_EXISTS_EXCEPTION if a manager already exists for this
 *       ByteArray.</listitem>
 *   <listitem>SHARE_NAME_IN_USE_EXCEPTION if a Client with this name is already
 *	 joined to this ByteArray.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if the remote host associated with this
 *	 Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *	 permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *	 in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_create_byte_array_with_manager(ShareSession *session,
                                             ShareClient *client,
                                             gchar *byte_array_name,
                                             gboolean auto_join,
                                             ShareByteArrayManager *manager,
                                             ShareByteArray *byte_array)
{
    printf("share_session_create_byte_array_with_manager called.\n");
}


/**
 * share_session_create_channel:
 * @session: the Session.
 * @client: a client that will be used for authentication purposes if 
 *        this is a managed session.
 * @channel_name: the name to give this channel.
 * @reliable: whether the channel is reliable. In other words whether the
 *        channel uses networking technology that reliably delivers data
 *        sent over it (such as TCP), or uses unreliable technology, which 
 *        possibly could lose such data (such as UDP). It does not guarantee 
 *        to reliably deliver Data messages sent over a Channel.
 * @ordered: whether data sent over the channel is ordered.
 * @auto_join: if true, automatically join the Channel when it's created.
 * @channel: the newly created Channel or a local reference to it, if 
 *        it was already created.
 *
 * Creates a Channel with the given name which can then be used with the
 * various Channel operations. This Channel is then known to the Session.
 * No ChannelManager is associated with this Channel; any Client which has
 * already joined the Session may freely join this Channel.
 *
 * If a Channel with this name already exists, a reference to that Channel
 * is returned.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NAME_IN_USE_EXCEPTION if a Client with this name is already
 *	 joined to this ByteArray.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if the remote host associated with this
 *	 Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *	 permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *	 in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_create_channel(ShareSession *session,
                             ShareClient *client,
                             gchar *channel_name,
                             gboolean reliable,
                             gboolean ordered,
                             gboolean auto_join,
                             ShareChannel *channel)
{
    printf("share_session_create_channel called.\n");
}


/**
 * share_session_create_channel_with_manager:
 * @session: the Session.
 * @client: a Client that will be used for authentication purposes if 
 *        this is a managed Session.
 * @channel_name: the name to give this Channel.
 * @reliable: whether the channel is reliable. In other words whether the
 *        channel uses networking technology that reliably delivers data
 *        sent over it (such as TCP), or uses unreliable technology, which 
 *        possibly could lose such data (such as UDP). It does not guarantee 
 *        to reliably deliver Data messages sent over a Channel.
 * @ordered: whether Data sent over the Channel is ordered.
 * @auto_join: if true, automatically join the Channel when it's created.
 * @manager: the manager for this Channel.
 * @channel: the newly created managed Channel.
 *
 * Creates a Channel with the given name which can then be used with the
 * various Channel operations. This Channel is then known to the Session.
 * A ChannelManager is associated with this Channel; Clients are
 * authenticated before they are allowed to join the Channel (ie. the
 * equivalent of a private channel).
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_MANAGER_EXISTS_EXCEPTION if a manager already exists for this
 *       Channel.</listitem>
 *   <listitem>SHARE_NAME_IN_USE_EXCEPTION if a Client with this name is already
 *       joined to this ByteArray.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if the remote host associated with this
 *       Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *       permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_create_channel_with_manager(ShareSession *session,
                                          ShareClient *client,
                                          gchar *channel_name,
                                          gboolean reliable,
                                          gboolean ordered,
                                          gboolean auto_join,
                                          ShareChannelManager *manager,
                                          ShareChannel *channel)
{
    printf("share_session_create_channel_with_manager called.\n");
}

 
/**
 * share_session_create_token:
 * @session: the Session.
 * @client: a Client that will be used for authentication purposes if 
 *        this is a managed Session.
 * @token_name: the name to give this Token.
 * @auto_join: if true, automatically join the Token when it's created.
 * @token: the newly created Token or a local reference to it, if it was
 *        already created.
 *
 * Creates a Token with the given name which can then be used with the
 * various Token operations. This Token is then known to the session. No
 * TokenManager is associated with this Token; any Client which has
 * already joined the Session may freely join this Token.
 *
 * If a Token with this name already exists, a reference to that Token is
 * returned.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NAME_IN_USE_EXCEPTION if a Client with this name is already
 *       joined to this ByteArray.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if the remote host associated with this
 *       Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *       permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_create_token(ShareSession *session,
                           ShareClient *client,
                           gchar *token_name,
                           gboolean auto_join,
                           ShareToken *token)
{
    printf("share_session_create_token called.\n");
}

 
/**
 * share_session_create_token_with_manager:
 * @session: the Session.
 * @client: a Client that will be used for authentication purposes if 
 *        this is a managed Session.
 * @token_name: the name of the Token to create.
 * @auto_join: if true, automatically join the Token when it's created.
 * @manager: the manager of this Token.
 * @token: the newly created managed Token.
 *
 * Creates a Token with the given name which can then be used with the
 * various Token operations. This Token is then known to the Session.
 * A TokenManager is associated with this Token; Clients are authenticated
 * before they are allowed to join the Token (ie. the equivalent of a
 * private Token).
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_MANAGER_EXISTS_EXCEPTION if a manager already exists for this
 *       Token.</listitem>
 *   <listitem>SHARE_NAME_IN_USE_EXCEPTION if a Client with this name is already
 *	 joined to this ByteArray.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if the remote host associated with this
 *	 Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *	 permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *	 in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_create_token_with_manager(ShareSession *session,
                                        ShareClient *client,
                                        gchar *token_name,
                                        gboolean auto_join,
                                        ShareTokenManager *manager,
                                        ShareToken *token)
{
    printf("share_session_create_token_with_manager called.\n");
}

 
/**
 * share_session_byte_array_exists:
 * @session: the Session.
 * @byte_array_name: the name of the ByteArray to check on.
 * @exists: return true if a ByteArray with this name exists; false if 
 *        it doesn't.
 *
 * Checks if a Bytearray with this name exists.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_byte_array_exists(ShareSession *session,
                                gchar *byte_array_name,
                                gboolean *exists)
{
    printf("share_session_byte_array_exists called.\n");
}

 
/**
 * share_session_channel_exists:
 * @session: the Session.
 * @channel_name: the name of the Channel to check on.
 * @exists: return true if a Channel with this name exists; false if 
 *        it doesn't.
 *
 * Checks if a Channel with this name exists.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_channel_exists(ShareSession *session,
                             gchar *channel_name,
                             gboolean *exists)
{
    printf("share_session_channel_exists called.\n");
}


/**
 * share_session_token_exists:
 * @session: the Session.
 * @token_name: the name of the Token to check on.
 * @exists: return true if a Token with this name exists; false if 
 *        it doesn't.
 *
 * Checks if a Token with this name exists.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_token_exists(ShareSession *session,
                           gchar *token_name,
                           gboolean *exists)
{
    printf("share_session_token_exists called.\n");
}


/**
 * share_session_byte_array_managed
 * @session: the Session.
 * @byte_array_name: the name of the ByteArray to check on.
 * @managed: returns true if the ByteArray with this name is managed;
 *        false if it isn't.
 *
 * Checks if the Bytearray with this name is managed.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if the byte array doesn't 
 *       exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *	 in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_byte_array_managed(ShareSession *session,
                                 gchar *byte_array_name,
                                 gboolean *managed)
{
    printf("share_session_byte_array_managed called.\n");
}


/**
 * share_session_channel_managed:
 * @session: the Session.
 * @channel_name: the name of the Channel to check on.
 * @managed: return true if the Channel with this name is managed; 
 *        false if it isn't.
 *
 * Checks if the Channel with this name is managed.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if the channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_channel_managed(ShareSession *session,
                              gchar *channel_name,
                              gboolean *managed)
{
    printf("share_session_channel_managed called.\n");
}


/**
 * share_session_token_managed:
 * @session: the Session.
 * @token_name: the name of the Token to check on.
 * @managed: return true if the Token with this name is managed; 
 *        false if it isn't.
 *
 * Checks if the Token with this name is managed.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if the token doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_token_managed(ShareSession *session,
                            gchar *token_name,
                            gboolean *managed)
{
    printf("share_session_token_managed called.\n");
}


/**
 * share_session_get_byte_arrays_joined:
 * @session: the Session.
 * @client: the Client to check on.
 * @byte_arrays: an array of ByteArrays that this Client has 
 *        successfully joined. This array will be of zero length if this 
 *        Client has not joined any ByteArrays.
 *
 * Return an array of ByteArrays that this Client has successfully joined.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_get_byte_arrays_joined(ShareSession *session,
                                     ShareClient *client,
                                     ShareByteArray *byte_arrays[])
{
    printf("share_session_get_byte_arrays_joined called.\n");
}


/**
 * share_session_get_channels_joined:
 * @session: the Session.
 * @client: the Client to check on.
 * @channels: an array of Channels that this Client has successfully 
 *        joined. This array will be of zero length if this Client has not 
 *        joined any Channels.
 *
 * Return an array of Channels that this Client has successfully joined.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_get_channels_joined(ShareSession *session,
                                  ShareClient *client,
                                  ShareChannel *channels[])
{
    printf("share_session_get_channels_joined called.\n");
}


/**
 * share_session_get_tokens_joined:
 * @session: the Session.
 * @client: the Client to check on.
 * @tokens: an array of Tokens that this Client has successfully joined.
 *        This array will be of zero length if this Client has not joined 
 *        any Tokens.
 *
 * Return an array of Tokens that this Client has successfully joined.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_get_tokens_joined(ShareSession *session,
                                ShareClient *client,
                                ShareToken *tokens[])
{
    printf("share_session_get_tokens_joined called.\n");
}


/**
 * share_session_list_byte_array_names:
 * @session: the Session.
 * @names: a sorted array of names of the ByteArrays that are known to
 *        this Session. This array will be of zero length if there are no 
 *        ByteArrays created in this Session.
 *
 * List the names of the ByteArrays that are known to this Session.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_list_byte_array_names(ShareSession *session,
                                    gchar *names[])
{
    printf("share_session_list_byte_array_names called.\n");
}


/**
 * share_session_list_channel_names:
 * @session: the Session.
 * @names: a sorted array of names of the Channels that are known to
 *        this Session. This array will be of zero length if there are no 
 *        Channels created in this Session.
 *
 * List the names of the Channels that are known to this Session.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_list_channel_names(ShareSession *session,
                                 gchar *names[])
{
    printf("share_session_list_channel_names called.\n");
}


/**
 * share_session_list_token_names:
 * @session: the Session.
 * @names: a sorted array of names of the Tokens that are known to
 *        this Session. This array will be of zero length if there are no 
 *        Tokens created in this Session.
 *
 * List the names of the Tokens that are known to this Session.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_list_token_names(ShareSession *session,
                               gchar *names[])
{
    printf("share_session_list_token_names called.\n");
}


/**
 * share_session_get_url:
 * @session: the Session.
 *
 * Gives the URLString used to create this Session.
 *
 * Returns: the URLString used to create this Session.
 */

ShareUrlString * 
share_session_get_url(ShareSession *session)
{
    printf("share_session_get_url called.\n");
}


/**
 * share_session_close:
 * @session: the Session.
 * @close_connection: indicates whether the underlying connection
 *        used by this application should be forcefully closed. Forcefully
 *        closing this connection would automatically render all references 
 *        to other Sessions on the "host:port" being used by this Session 
 *        as invalid.
 *
 * Closes the session, rendering the session handle invalid. This method
 * should be called when an application terminates to facilitate the cleanup
 * process. If a client wishes to further participate in this session, then
 * it will need to get a new session handle with
 * share_session_factory_create_session()
 *
 * If there are no other Sessions on the same "host:port" as this Session,
 * the underlying connection being used by this applet or application will
 * automatically be closed.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_session_close(ShareSession *session,
                    gboolean close_connection)
{
    printf("share_session_close called.\n");
}
