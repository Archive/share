
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#include <share/share_object.h>

static void share_object_class_init     (ShareObjectClass *class);
static void share_object_instance_init  (ShareObject      *instance);


GType 
share_object_get_type(void)
{
    static GType object_type = 0;

    if (!object_type) {
        static const GTypeInfo object_info = {
            sizeof(ShareObjectClass),
            NULL,			/* base_init */
            NULL,			/* base_finalize */
            (GClassInitFunc) share_object_class_init,
            NULL,			/* class_finalize */
            NULL,			/* class_data */
            sizeof(ShareObject),
            0,				/* n_preallocs */
            (GInstanceInitFunc) share_object_instance_init,
        };

        object_type = g_type_register_static(G_TYPE_OBJECT,
                                             "ShareObject",
                                             &object_info, 0);
    }

    return(object_type);
}


static void 
share_object_class_init(ShareObjectClass *class)
{
    printf("share_object_class_init called.\n");
}


static void 
share_object_instance_init(ShareObject*instance)
{
    printf("share_object_instance_init called.\n");
}


ShareObject * 
share_object_new(void)
{
    printf("share_object_new called.\n");
}
