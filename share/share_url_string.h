
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_URL_STRING_H__
#define __SHARE_URL_STRING_H__

#include <share/share_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_URL_STRING \
            (share_url_string_get_type())

#define SHARE_URL_STRING(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_URL_STRING, ShareUrlString))

#define SHARE_URL_STRING_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_URL_STRING, ShareUrlStringClass))

#define SHARE_IS_URL_STRING(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_URL_STRING))

#define SHARE_IS_URL_STRING_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_URL_STRING))

#define SHARE_URL_STRING_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_URL_STRING, ShareUrlStringClass))

typedef struct _ShareUrlString      ShareUrlString;
typedef struct _ShareUrlStringClass ShareUrlStringClass;

struct _ShareUrlString
{
    ShareObject object;

    gchar* protocol;
    gchar* host_name;
    gchar* host_address;
    gint port;
    gchar* connect_type;
    gchar* object_type;
    gchar* object_name;
    gboolean valid;
};

struct _ShareUrlStringClass
{
    ShareObjectClass parent_class;
};


GType       
share_url_string_get_type             (void);

ShareUrlString * 
share_url_string_new_session_url      (gchar *host_name,
                                       gint port,
                                       gchar *connection_type,
                                       gchar *session_name);

ShareUrlString * 
share_url_string_new_client_url       (gchar *host_name,
                                       gint port, 
                                       gchar *connection_type, 
                                       gchar *client_name);

gchar * 
share_url_string_get_host_address     (ShareUrlString *url_string);

gchar * 
share_url_string_get_host_name        (ShareUrlString *url_string);

gchar * 
share_url_string_get_protocol         (ShareUrlString *url_string);

gint    
share_url_string_get_port             (ShareUrlString *url_string);

gchar * 
share_url_string_get_connection_type  (ShareUrlString *url_string);

gchar * 
share_url_string_get_object_type      (ShareUrlString *url_string);

gchar * 
share_url_string_get_object_name      (ShareUrlString *url_string);

gboolean 
share_url_string_is_valid             (ShareUrlString *url_string);

gboolean 
share_url_string_equals               (ShareUrlString *url_string,
                                       GObject *object);

gchar * 
share_url_string_to_string            (ShareUrlString *url_string);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_URL_STRING_H__ */
