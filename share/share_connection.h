
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_CONNECTION_H__
#define __SHARE_CONNECTION_H__

#include <share/share_types.h>
#include <share/share_connection_event.h>
#include <share/share_properties.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_CONNECTION \
            (share_connection_get_type())

#define SHARE_CONNECTION(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_CONNECTION, ShareConnection))

#define SHARE_CONNECTION_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_CONNECTION, ShareConnectionClass))

#define SHARE_IS_CONNECTION(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_CONNECTION))

#define SHARE_IS_CONNECTION_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_CONNECTION))

#define SHARE_CONNECTION_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_CONNECTION, ShareConnectionClass))

typedef struct _ShareConnection      ShareConnection;
typedef struct _ShareConnectionClass ShareConnectionClass;

struct _ShareConnection
{
    ShareObject object;
};

struct _ShareConnectionClass
{
    ShareObjectClass parent_class;

    void (* share_connection_failed)  (ShareConnection *connection,
                                       ShareConnectionEvent *event);
};


GType 
share_connection_get_type                (void);

ShareConnection * 
share_connection_new                     (void);

ShareProperties * 
share_connection_get_properties          (ShareConnection *connection);

gchar * 
share_connection_get_property            (ShareConnection *connection,
                                          gchar * key);

void 
share_connection_set_properties          (ShareConnection *connection,
                                          ShareProperties * properties);

GObject * 
share_connection_set_property            (ShareConnection *connection,
                                          gchar *key,
                                          gchar *value);

GObject * 
share_connection_remove_property         (ShareConnection *connection,
                                          gchar * key);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_CONNECTION_H__ */
