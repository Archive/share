
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_TOKEN_H__
#define __SHARE_TOKEN_H__

#include <share/share_types.h>
#include <share/share_event.h>
#include <share/share_client.h>
#include <share/share_manageable.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_TOKEN \
            (share_token_get_type())

#define SHARE_TOKEN(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_TOKEN, ShareToken))

#define SHARE_TOKEN_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_TOKEN, ShareTokenClass))

#define SHARE_IS_TOKEN(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_TOKEN))

#define SHARE_IS_TOKEN_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_TOKEN))

#define SHARE_TOKEN_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_TOKEN, ShareTokenClass))

typedef struct _ShareTokenClass ShareTokenClass;

struct _ShareToken
{
    ShareManageable manageable;
};

struct _ShareTokenClass
{
    ShareManageableClass parent_class;

    void (* share_token_expelled)    (ShareToken *token,
                                      ShareTokenEvent *event);

    void (* share_token_given)       (ShareToken *token,
                                      ShareTokenEvent *event);

    void (* share_token_grabbed)     (ShareToken *token,
                                      ShareTokenEvent *event);

    void (* share_token_invited)     (ShareToken *token,
                                      ShareTokenEvent *event);

    void (* share_token_joined)      (ShareToken *token,
                                      ShareTokenEvent *event);

    void (* share_token_left)        (ShareToken *token,
                                      ShareTokenEvent *event);

    void (* share_token_released)    (ShareToken *token,
                                      ShareTokenEvent *event);

    void (* share_token_requested)   (ShareToken *token,
                                      ShareTokenEvent *event);

};


/** Token not in use state. */
static int SHARE_NOT_IN_USE        = 100;

/** Token grabbed state. */
static int SHARE_GRABBED           = 101;

/** Token inhibited state. */
static int SHARE_INHIBITED         = 102;

/** Token giving state. */
static int SHARE_GIVING            = 103;

/** Token already grabbed state. */
static int SHARE_ALREADY_GRABBED   = 104;

/** Token already inhibited state. */
static int SHARE_ALREADY_INHIBITED = 105;

GType    
share_token_get_type          (void);

ShareToken * 
share_token_new               (void);

ShareStatus 
share_token_give              (ShareToken *token,
                               ShareClient *client,
                               gchar *receiving_client_name,
                               gint *token_status);

ShareStatus 
share_token_grab              (ShareToken *token,
                               ShareClient *client,
                               gboolean exclusive,
                               gint *token_status);

ShareStatus 
share_token_list_holder_names (ShareToken *token,
                               gchar *names[]);

ShareStatus 
share_token_request           (ShareToken *token,
                               ShareClient *client,
                               gint *token_status);

ShareStatus 
share_token_release           (ShareToken *token,
                               ShareClient *client,
                               gint *token_status);

ShareStatus 
share_token_test              (ShareToken *token,
                               gint *token_status);
 
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_TOKEN_H__ */
