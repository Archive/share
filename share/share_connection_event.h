
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_CONNECTION_EVENT_H__
#define __SHARE_CONNECTION_EVENT_H__

#include <share/share_event.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_CONNECTION_EVENT \
            (share_connection_event_get_type())

#define SHARE_CONNECTION_EVENT(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_CONNECTION_EVENT, ShareConnectionEvent))

#define SHARE_CONNECTION_EVENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_CONNECTION_EVENT, ShareConnectionEventClass))

#define SHARE_IS_CONNECTION_EVENT(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_CONNECTION_EVENT))

#define SHARE_IS_CONNECTION_EVENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_CONNECTION_EVENT))

#define SHARE_CONNECTION_EVENT_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_CONNECTION_EVENT, ShareConnectionEventClass))

typedef struct _ShareConnectionEvent        ShareConnectionEvent;
typedef struct _ShareConnectionEventClass   ShareConnectionEventClass;

struct _ShareConnectionEvent
{
    ShareEvent event;
};

struct _ShareConnectionEventClass
{
    ShareEventClass parent_class;
};


GType 
share_connection_event_get_type              (void);

ShareConnectionEvent * 
share_connection_event_new                   (gchar *address,
                                              gint port,
                                              gint type);

gchar * 
share_connection_event_get_address           (ShareConnectionEvent *event);

gint 
share_connection_event_get_port              (ShareConnectionEvent *event);

gchar * 
share_connection_event_to_string             (ShareConnectionEvent *event);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_CONNECTION_EVENT_H__ */
