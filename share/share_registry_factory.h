
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.          See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_REGISTRY_FACTORY_H__
#define __SHARE_REGISTRY_FACTORY_H__

#include <share/share_types.h>
#include <share/share_manager.h>
#include <share/share_registry_event.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_REGISTRY_FACTORY \
            (share_registry_factory_get_type())

#define SHARE_REGISTRY_FACTORY(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_REGISTRY_FACTORY, ShareRegistryFactory))

#define SHARE_REGISTRY_FACTORY_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_REGISTRY_FACTORY, ShareRegistryFactoryClass))

#define SHARE_IS_REGISTRY_FACTORY(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_REGISTRY_FACTORY))

#define SHARE_IS_REGISTRY_FACTORY_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_REGISTRY_FACTORY))

#define SHARE_REGISTRY_FACTORY_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_REGISTRY_FACTORY, ShareRegistryFactoryClass))

typedef struct _ShareRegistryFactory      ShareRegistryFactory;
typedef struct _ShareRegistryFactoryClass ShareRegistryFactoryClass;

struct _ShareRegistryFactory
{
    ShareObject object;
};

struct _ShareRegistryFactoryClass
{
    ShareObjectClass parent_class;

    void (* share_client_created)     (ShareRegistryFactory *registry_factory,
                                       ShareRegistryEvent *event);

    void (* share_client_destroyed)   (ShareRegistryFactory *registry_factory,
                                       ShareRegistryEvent *event);

    void (* share_connection_failed)  (ShareRegistryFactory *registry_factory,
                                       ShareRegistryEvent *event);

    void (* share_session_created)    (ShareRegistryFactory *registry_factory,
                                       ShareRegistryEvent *event);

    void (* share_session_destroyed)  (ShareRegistryFactory *registry_factory,
                                       ShareRegistryEvent *event);

};


GType    
share_registry_factory_get_type        (void);

ShareStatus 
share_registry_factory_registry_exists (gchar *registry_type,
                                        gboolean *exists);

ShareStatus 
share_registry_factory_registry_exists_with_port
                                       (gchar *registry_type,
                                        gint port,
                                        gboolean *exists);

ShareStatus 
share_registry_factory_list            (ShareUrlString *names[]);

ShareStatus 
share_registry_factory_list_on_host    (gchar *host,
                                        gchar *registry_type,
                                        ShareUrlString *names[]);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_REGISTRY_FACTORY_H__ */
