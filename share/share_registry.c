
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.          See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#include <share/share_registry.h>

static void share_registry_class_init	  (ShareRegistryClass *class);
static void share_registry_instance_init  (ShareRegistry      *instance);

static ShareObjectClass *parent_class = NULL;


GType    
share_registry_get_type(void)
{
    static GType registry_type = 0;

    if (!registry_type) {
	static const GTypeInfo registry_info = {
            sizeof(ShareRegistryClass),
            NULL,			/* base_init */
            NULL,			/* base_finalize */
            (GClassInitFunc) share_registry_class_init,
            NULL,			/* class_finalize */
            NULL,			/* class_data */
            sizeof(ShareRegistry),
            0,				/* n_preallocs */
            (GInstanceInitFunc) share_registry_instance_init,
	};

	registry_type = g_type_register_static(SHARE_TYPE_OBJECT,
                                               "ShareRegistry",
                                               &registry_info, 0);
    }

    return(registry_type);
}


static void 
share_registry_class_init(ShareRegistryClass *class)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS(class);

    parent_class = g_type_class_peek_parent(class);
}


static void 
share_registry_instance_init(ShareRegistry *instance)
{
    printf("share_registry_instance_init called.\n");
}


ShareRegistry * 
share_registry_new(gchar *registry_type)
{
    ShareRegistry *registry;

    registry = g_object_new(share_registry_get_type(), NULL);

    return(registry);
}


ShareRegistry *
share_registry_new_with_port(gchar *registry_type,
                             gint port)
{
    printf("share_registry_new_with_port called.\n");
}


ShareRegistry *
share_registry_new_with_manager(gchar *registry_type,
                                ShareManager *manager)
{
    printf("share_registry_new_with_manager called.\n");
}


/**
 * share_registry_start_registry:
 * @registry: the Registry.
 *
 * Start a Registry. The Registry is started in a separate thread. It can 
 * be stopped with share_registry_factory_stop_registry. If the process 
 * that started it terminates, then the Registry thread is terminated too.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_REGISTRY_EXISTS_EXCEPTION if a Registry (or some other process)
 *       is already running on the port used by the Registry on this 
 *       machine.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if an invalid registry type was 
 *       given.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_registry_start_registry(ShareRegistry *registry)
{
    printf("share_registry_start_registry called.\n");
}


/**
 * share_registry_stop_registry:
 * @registry: the Registry.
 *
 * Stop a Registry.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if an invalid registry type was 
 *       given.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_registry_stop_registry(ShareRegistry *registry)
{
    printf("share_registry_stop_registry called.\n");
}
