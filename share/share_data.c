
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The Data object.
 */

#include <share/share_data.h>

static void share_data_class_init     (ShareDataClass *class);
static void share_data_instance_init  (ShareData      *instance);


GType     
share_data_get_type(void)
{
    printf("share_data_get_type called.\n");
}


static void 
share_data_class_init(ShareDataClass *class)
{
    printf("share_data_class_init called.\n");
}


static void 
share_data_instance_init(ShareData *instance)
{
    printf("share_data_instance_init called.\n");
}


/**
 * share_data_new:
 * @data: the data (an array of bytes).
 *
 * Constructor for the Data class. An array of bytes is turned into a
 * Data object. The length of the data array is the length of the data object.
 *
 * Returns: a ShareData object.
 */

ShareData *    
share_data_new(byte data[])
{
    printf("share_data_new called.\n");
}


/**
 * share_data_new_with_length:
 * @data: the data (an array of bytes).
 * @length: the length of the data.
 *
 * Constructor for the Data class. An array of bytes is turned into a Data
 * object. The length of the data object is provided. If this value is
 * invalid, then the length of the data object is the length of the array
 * of bytes.
 *
 * Returns: a ShareData object.
 */

ShareData *    
share_data_new_with_length(byte data[],
                           gint length)
{
    printf("share_data_new_with_length called.\n");
}


/**
 * share_data_new_with_string:
 * @string: the data (a String).
 *
 * Constructor for the Data class. A String is turned into a Data object.
 * The length of the data array is the length of the String object.
 *
 * Returns: a ShareData object.
 */

ShareData *    
share_data_new_with_string(gchar *string)
{
    printf("share_data_new_with_string called.\n");
}


/**
 * share_data_new_with_object:
 * @object: the data (a GObject).
 *
 * Constructor for the Data class. A GObject is turned into a Data object.
 * The length of the data array is the length of the serialized GObject.
 *
 * Returns: a ShareData object.
 */

ShareData *    
share_data_new_with_object(GObject *object)
{
    printf("share_data_new_with_object called.\n");
}


/**
 * share_data_get_data_as_bytes:
 * @data: the Data object.
 *
 * Get the data associated with this Data object, as an array of bytes.
 *
 * Returns: the data contained in this Data object, as an array of bytes.
 */

byte *       
share_data_get_data_as_bytes(ShareData *data)
{
    printf("share_data_get_data_as_bytes called.\n");
}


/**
 * share_data_get_data_as_object:
 * @data: the Data object.
 *
 * Get the data associated with this Data object, as a GObject.
 *
 * Returns: the data contained in this Data object, as a GObject.
 */

GObject *  
share_data_get_data_as_object(ShareData *data)
{
    printf("share_data_get_data_as_object called.\n");
}


/**
 * share_data_get_data_as_string:
 * @data: the Data object.
 *
 * Get the data associated with this Data object, as a string.
 *
 * Returns: the data contained in this Data object, as a string.
 */

gchar *  
share_data_get_data_as_string(ShareData *data)
{
    printf("share_data_get_data_as_string called.\n");
}


/**
 * share_data_get_length:
 * @data: the Data object.
 *
 * Get the length of the data in this Data object.
 *
 * Returns: the length (in bytes) of the data in this Data object.
 */

gint        
share_data_get_length(ShareData *data)
{
    printf("share_data_get_length called.\n");
}


/**
 * share_data_get_sender_name:
 * @data: the Data object.
 *
 * Get the name of the Data sender.
 *
 * Returns: the name of the sender of this Data.
 */

gchar *  
share_data_get_sender_name(ShareData *data)
{
    printf("share_data_get_sender_name called.\n");
}


/**
 * share_data_get_priority:
 * @data: the Data object.
 *
 * Gets the priority that this Data was sent at.
 *
 * Returns: the priority that this Data was sent at.
 */

gint        
share_data_get_priority(ShareData *data)
{
    printf("share_data_get_priority called.\n");
}

 
/**
 * share_data_set_priority:
 * @data: the Data object.
 * @priority: the new priority value.
 *
 * Set a new priority value for this data.
 */

void        
share_data_set_priority(ShareData *data,
                        gint priority)
{
    printf("share_data_set_priority called.\n");
}

 
/**
 * share_data_get_channel:
 * @data: the Data object.
 *
 * Get the Channel that this data was sent over.
 *
 * Returns: the Channel that this Data was sent over.
 */

ShareChannel *  
share_data_get_channel(ShareData *data)
{
    printf("share_data_get_channel called.\n");
}

 
/**
 * share_data_set_channel:
 * @data: the Data object.
 * @channel: the new channel value.
 *
 * Set a new channel value for this data.
 */

void        
share_data_set_channel(ShareData *data,
                       ShareChannel *channel)
{
    printf("share_data_set_channel called.\n");
}

 
/**
 * share_data_set_sender_name:
 * @data: the Data object.
 * @sender_name: the new sender name.
 *
 * Set a new sender name for this data.
 */

void        
share_data_set_sender_name(ShareData *data,
                           gchar *sender_name)
{
    printf("share_data_set_sender_name called.\n");
}
