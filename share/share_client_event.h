
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_CLIENT_EVENT_H__
#define __SHARE_CLIENT_EVENT_H__

#include <share/share_event.h>
#include <share/share_client.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_CLIENT_EVENT \
            (share_client_event_get_type())

#define SHARE_CLIENT_EVENT(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_CLIENT_EVENT, ShareClientEvent))

#define SHARE_CLIENT_EVENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_CLIENT_EVENT, ShareClientEventClass))

#define SHARE_IS_CLIENT_EVENT(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_CLIENT_EVENT))

#define SHARE_IS_CLIENT_EVENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_CLIENT_EVENT))

#define SHARE_CLIENT_EVENT_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_CLIENT_EVENT, ShareClientEventClass))

typedef struct _ShareClientEvent        ShareClientEvent;
typedef struct _ShareClientEventClass   ShareClientEventClass;

struct _ShareClientEvent
{
    ShareEvent event;
};

struct _ShareClientEventClass
{
    ShareEventClass parent_class;
};


/** The Client ByteArray invited event type. */
static int SHARE_CLIENT_BYTEARRAY_INVITED  = 1 << 0;

/** The Client ByteArray expelled event type. */
static int SHARE_CLIENT_BYTEARRAY_EXPELLED = 1 << 1;

/** The Client Channel invited event type. */
static int SHARE_CLIENT_CHANNEL_INVITED    = 1 << 2;

/** The Client Channel expelled event type. */
static int SHARE_CLIENT_CHANNEL_EXPELLED   = 1 << 3;

/** The Client Session invited event type. */
static int SHARE_CLIENT_SESSION_INVITED    = 1 << 4;

/** The Client Session expelled event type. */
static int SHARE_CLIENT_SESSION_EXPELLED   = 1 << 5;

/** The Client Token invited event type. */
static int SHARE_CLIENT_TOKEN_INVITED      = 1 << 6;

/** The Client Token expelled event type. */
static int SHARE_CLIENT_TOKEN_EXPELLED     = 1 << 7;

/** The Client Token given event type. */
static int SHARE_CLIENT_TOKEN_GIVEN        = 1 << 8;

GType 
share_client_event_get_type               (void);

ShareClientEvent * 
share_client_event_new                    (ShareSession *session,
                                           ShareClient *client,
                                           gchar *resource_name,
                                           gint type);

gchar * 
share_client_event_get_resource_name      (ShareClientEvent *event);

ShareSession * 
share_client_event_get_session            (ShareClientEvent *event);

gchar * 
share_client_event_to_string              (ShareClientEvent *event);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_CLIENT_EVENT_H__ */
