
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The Session Manager object.
 */

#include <share/share_session_manager.h>

static void share_session_manager_class_init     (ShareSessionManagerClass *class);
static void share_session_manager_instance_init  (ShareSessionManager *instance);


GType 
share_session_manager_get_type(void)
{
    printf("share_session_manager_get_type called.\n");
}


static void 
share_session_manager_class_init(ShareSessionManagerClass *class)
{
    printf("share_session_manager_class_init called.\n");
}


static void 
share_session_manager_instance_init(ShareSessionManager *instance)
{
    printf("share_session_manager_instance_init called.\n");
}


ShareSessionManager * 
share_session_manager_new(void)
{
    printf("share_session_manager_new called.\n");
}


/**
 * share_session_manager_channel_request:
 * @session_manager: the Session Manager.
 * @session: the Session the Client is interested in.
 * @info: the authentication information.
 * @client: the Client.
 *
 * Called when there is a Client interested in performing a priviledged
 * operation on a managed Session.
 *
 * The following priviledged operations could occur:
 * <itemizedlist>
 *   <listitem>CREATE a ByteArray within the managed Session.</listitem>
 *   <listitem>DESTROY a ByteArray within the managed Session.</listitem>
 *   <listitem>CREATE a Channel within the managed Session.</listitem>
 *   <listitem>DESTROY a Channel within the managed Session.</listitem>
 *   <listitem>CREATE a Token within the managed Session.</listitem>
 *   <listitem>DESTROY a Token within the managed Session.</listitem>
 *   <listitem>JOIN a managed Session.</listitem>
 * </itemizedlist>
 *
 * See: share_authentication_info
 *
 * Returns: true if the client is allowed to perform the priviledged operation;
 *         false if not.
 */

gboolean 
share_session_manager_channel_request(ShareSessionManager *session_manager,
                                      ShareSession *session,
                                      ShareAuthenticationInfo *info,
                                      ShareClient *client)
{
    printf("share_session_manager_channel_request called.\n");
}
