
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The Connection event. Connection events are created for the following
 * actions:
 * <itemizedlist>
 *   <listitem>when a Connection failure has occured.</listitem>
 * </itemizedlist>
 */

#include <share/share_connection_event.h>

static void share_connection_event_class_init     (ShareConnectionEventClass *class);
static void share_connection_event_instance_init  (ShareConnectionEvent	*instance);


GType 
share_connection_event_get_type(void)
{
    printf("share_connection_event_get_type called.\n");
}


static void 
share_connection_event_class_init(ShareConnectionEventClass *class)
{
    printf("share_connection_event_class_init called.\n");
}


static void 
share_connection_event_instance_init(ShareConnectionEvent *instance)
{
    printf("share_connection_event_instance_init called.\n");
}


/**
 * share_connection_event_new:
 * @address: the host address of the connection failure.
 * @port: the port number that the connection failure occured on.
 * @type: the type of event.
 *
 * Constructor for the ConnectionEvent object. A new connection event is
 * generated for a failure on (one of) the underlying connection(s).
 *
 * Returns: a ShareConnectionEvent object.
 */

ShareConnectionEvent * 
share_connection_event_new(gchar *address,
                           gint port,
                           gint type)
{
    printf("share_connection_event_new called.\n");
}


/**
 * share_connection_event_get_address:
 * @event: the Connection event.
 *
 * Get the host address that generated this event. This will be an IP
 * address (either unicast or multicast depending upon the implementation
 * type being used.
 *
 * Returns: the host address that generated this event.
 */

gchar * 
share_connection_event_get_address(ShareConnectionEvent *event)
{
    printf("share_connection_event_get_address called.\n");
}


/**
 * share_connection_event_get_port:
 * @event: the Connection event.
 *
 * Get the port number that generated this event.
 *
 * Returns: the port number that generated this event.
 */

gint 
share_connection_event_get_port(ShareConnectionEvent *event)
{
    printf("share_connection_event_get_port called.\n");
}


/**
 * share_connection_event_to_string:
 * @event: the Connection event.
 *
 * Return a short description of this Connection event.
 *
 * Returns: a String containing a description of this Connection event.
 */

gchar * 
share_connection_event_to_string(ShareConnectionEvent *event)
{
    printf("share_connection_event_to_string called.\n");
}
