
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.          See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * This is a factory class that starts and stops a Registry of the
 * appropriate type.
 * [XXX: add a note about the valid type when an implementation is complete].
 *
 * As with all types of collaborative environments, there needs to be some
 * way for each application to initially rendezvous, so that data can be
 * shared. The Share group rendezvous point is a Session object, and a special
 * URL String is used to describe that Session. This Session information
 * needs to be kept somewhere that is easily accessable to all applications.
 * This is where the Registry fits in.
 *
 * The Registry contains a transient database that maps these URL Strings to
 * Share objects. When the Registry is first started, its database is empty.
 * The names stored in the Registry are pure and are not parsed. A
 * collaborative service storing itself in the Registry may want to prefix
 * the name of the service by a package name (although this is not required),
 * to reduce name collisions.
 *
 * The Registry can also be used to store special Clients. This Client is
 * capable of being invited to join a Share Session.
 *
 * If the Registry is started by share_registry_factory_start_registry,
 * then it runs in its own thread. This is not a deamon thread, so
 * it should either be stopped with share_registry_factory_stop_registry, 
 * or it will be terminated when the application that started it, exits.
 *
 * The Registry needs to be started on a "well-known" port. This is
 * defined by the #registryPort property, which by default, is
 * 4561, irrespective of the implementation type.
 *
 * See:        share_session_factory
 * See:        share_client_factory
 */

#include <share/share_registry_factory.h>
#include <share/share_marshal.h>

enum {
    SHARE_CLIENT_CREATED_SIGNAL,
    SHARE_CLIENT_DESTROYED_SIGNAL,
    SHARE_CONNECTION_FAILED_SIGNAL,
    SHARE_SESSION_CREATED_SIGNAL,
    SHARE_SESSION_DESTROYED_SIGNAL,
    SHARE_REGISTRY_FACTORY_LAST_SIGNAL
};


static void share_registry_factory_class_init	  (ShareRegistryFactoryClass *class);
static void share_registry_factory_instance_init  (ShareRegistryFactory	*instance);

static guint registry_factory_signals[SHARE_REGISTRY_FACTORY_LAST_SIGNAL] = { 0 };
static ShareObjectClass *parent_class = NULL;


GType    
share_registry_factory_get_type(void)
{
    static GType registry_factory_type = 0;

    if (!registry_factory_type) {
	static const GTypeInfo registry_factory_info = {
            sizeof(ShareRegistryFactoryClass),
            NULL,			/* base_init */
            NULL,			/* base_finalize */
            (GClassInitFunc) share_registry_factory_class_init,
            NULL,			/* class_finalize */
            NULL,			/* class_data */
            sizeof(ShareRegistryFactory),
            0,				/* n_preallocs */
            (GInstanceInitFunc) share_registry_factory_instance_init,
	};

	registry_factory_type = g_type_register_static(SHARE_TYPE_OBJECT,
                                                     "ShareRegistryFactory",
                                                     &registry_factory_info, 0);
    }

    return(registry_factory_type);
}


static void 
share_registry_factory_class_init(ShareRegistryFactoryClass *class)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS(class);

    parent_class = g_type_class_peek_parent(class);

    registry_factory_signals[SHARE_CLIENT_CREATED_SIGNAL] =
	g_signal_new("share_client_created",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareRegistryFactoryClass,
                            share_client_created),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_REGISTRY_FACTORY,
            SHARE_TYPE_REGISTRY_EVENT);

    registry_factory_signals[SHARE_CLIENT_DESTROYED_SIGNAL] =
		g_signal_new("share_client_destroyed",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareRegistryFactoryClass,
                      		share_client_destroyed),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
	    SHARE_TYPE_REGISTRY_FACTORY,
            SHARE_TYPE_REGISTRY_EVENT);

    registry_factory_signals[SHARE_CONNECTION_FAILED_SIGNAL] =
		g_signal_new("share_connection_failed",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareRegistryFactoryClass,
                      		share_connection_failed),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
	    SHARE_TYPE_REGISTRY_FACTORY,
            SHARE_TYPE_REGISTRY_EVENT);

    registry_factory_signals[SHARE_SESSION_CREATED_SIGNAL] =
		g_signal_new("share_session_created",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareRegistryFactoryClass,
                      		share_session_created),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
	    SHARE_TYPE_REGISTRY_FACTORY,
            SHARE_TYPE_REGISTRY_EVENT);

    registry_factory_signals[SHARE_SESSION_DESTROYED_SIGNAL] =
		g_signal_new("share_session_destroyed",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareRegistryFactoryClass,
                      		share_session_destroyed),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
	    SHARE_TYPE_REGISTRY_FACTORY,
            SHARE_TYPE_REGISTRY_EVENT);
}


static void 
share_registry_factory_instance_init(ShareRegistryFactory *instance)
{
    printf("share_registry_factory_instance_init called.\n");
}


static ShareRegistryFactory * 
share_registry_factory_new(gchar *registry_type)
{
    ShareRegistryFactory *registry_factory;

    registry_factory = g_object_new(share_registry_factory_get_type(), NULL);

    return(registry_factory);
}


/**
 * share_registry_factory_registry_exists:
 * @registry_type: the type of Registry to check on.
 * @exists: set true if a Registry is already running; false if it isn't.
 *
 * Check if a Registry, of the given registry type, is already running.
 * It uses the current setting of the #registryPort variable to determine 
 * the port number it is running on.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if an invalid registry type was 
 *       given.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_registry_factory_registry_exists(gchar *registry_type,
                                       gboolean *exists)
{
    printf("share_registry_factory_registry_exists called.\n");
}


/**
 * share_registry_factory_registry_exists_with_port:
 * @registry_type: the type of Registry to check on.
 * @port: the port number that the Registry is running on.
 * @exists: set true if a Registry is already running; false if it isn't.
 *
 * Check if a Registry, of the given registry type, is already running on
 * the given port.
 *
 * The property #registryPort will be set to the given port number.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if an invalid registry type was 
 *       given.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_registry_factory_registry_exists_with_port(gchar *registry_type,
                                                 gint port,
                                                 gboolean *exists)
{
    printf("share_registry_factory_registry_exists_with_port called.\n");
}


/**
 * share_registry_factory_list:
 * @names: an array of URL strings of the names of all the known bound 
 *        Share objects, or a zero length array if there are no bound objects.
 *
 * Lists all the url strings of the known bound Share objects.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if an invalid registry type was 
 *       given.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *	 in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_registry_factory_list(ShareUrlString *names[])
{
    printf("share_registry_factory_list called.\n");
}


/**
 * share_registry_factory_list_on_host:
 * @host: host name of the machine to look for bound Share objects on.
 * @registry_type: the type of Registry.
 * @names: an array of URL strings of the names of all the known bound
 *        Share objects on the given host, or a zero length array if there 
 *        are no bound objects.
 *
 * Lists all the url strings of the known bound Share objects on the given
 * host with the given registry type.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_HOST_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_REGISTRY_EXCEPTION if an invalid registry type was 
 *       given.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_registry_factory_list_on_host(gchar *host,
                                    gchar *registry_type,
                                    ShareUrlString *names[])
{
    printf("share_registry_factory_list_on_host called.\n");
}
