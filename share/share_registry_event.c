
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The Registry event. Registry events are created for the following actions:
 * <itemizedlist>
 *   <listitem>when a Session has been created.</listitem>
 *   <listitem>when a Session has been destroyed.</listitem>
 *   <listitem>when a Client has been created.</listitem>
 *   <listitem>when a Client has been destroyed.</listitem>
 *   <listitem>when the connection to the registry fails.</listitem>
 * </itemizedlist>
 */

#include <share/share_registry_event.h>

static void share_registry_event_class_init    (ShareRegistryEventClass *class);static void share_registry_event_instance_init (ShareRegistryEvent *instance);


GType 
share_registry_event_get_type(void)
{
    printf("share_registry_event_get_type called.\n");
}


static void 
share_registry_event_class_init(ShareRegistryEventClass *class)
{
    printf("share_registry_event_class_init called.\n");
}


static void 
share_registry_event_instance_init(ShareRegistryEvent *instance)
{
    printf("share_registry_event_instance_init called.\n");
}


/**
 * share_registry_event_new:
 * @client_name: the name of the client.
 * @resource_name: the URL String for the resource within the registry 
 *                 that the event affects.
 * @address: the host address of the registry.
 * @port: the port number that the registry uses.
 * @type: the type of event.
 *
 * Constructor for the RegistryEvent object. A new registry event is
 * generated for a client action within the given registry.
 *
 * Returns: a ShareRegistryEvent object.
 */

ShareRegistryEvent * 
share_registry_event_new(gchar *client_name,
                         ShareUrlString *resource_name,
                         gchar *address,
                         gint port,
                         gint type)
{
    printf("share_registry_event_new called.\n");
}


/**
 * share_registry_event_get_address:
 * @event: the Registry event.
 *
 * Get the host address that generated this event. This will be an IP
 * address (either unicast or multicast depending upon the implementation
 * type being used.
 *
 * Returns: the host address that generated this event.
 */

gchar * 
share_registry_event_get_address(ShareRegistryEvent *event)
{
    printf("share_registry_event_get_address called.\n");
}


/**
 * share_registry_event_get_client_name:
 * @event: the Registry event.
 *
 * Get the name of the Client that generated this event.
 *
 * Returns: the name of the Client that generated this event.
 */

gchar * 
share_registry_event_get_client_name(ShareRegistryEvent *event)
{
    printf("share_registry_event_get_client_name called.\n");
}


/**
 * share_registry_event_get_port:
 * @event: the Registry event.
 *
 * Get the port number that generated this event.
 *
 * Returns: the port number that generated this event.
 */

gint 
share_registry_event_get_port(ShareRegistryEvent *event)
{
    printf("share_registry_event_get_port called.\n");
}


/**
 * share_registry_event_get_resource_name:
 * @event: the Registry event.
 *
 * Get the URL String for the resource for this event. The resource will
 * be Session or Client that has either been created or destroyed.
 *
 * Returns: the URL String for the resource for this event.
 */

ShareUrlString * 
share_registry_event_get_resource_name(ShareRegistryEvent *event)
{
    printf("share_registry_event_get_resource_name called.\n");
}


/**
 * share_registry_event_to_string:
 * @event: the Registry event.
 *
 * Return a short description of this Registry event.
 *
 * Returns: a String containing a description of this Registry event.
 */

gchar * 
share_registry_event_to_string(ShareRegistryEvent *event)
{
    printf("share_registry_event_to_string called.\n");
}
