
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_TOKEN_MANAGER_H__
#define __SHARE_TOKEN_MANAGER_H__

#include <share/share_manager.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_TOKEN_MANAGER \
            (share_token_manager_get_type())

#define SHARE_TOKEN_MANAGER(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_TOKEN_MANAGER, ShareTokenManager))

#define SHARE_TOKEN_MANAGER_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_TOKEN_MANAGER, ShareTokenManagerClass))

#define SHARE_IS_TOKEN_MANAGER(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_TOKEN_MANAGER))

#define SHARE_IS_TOKEN_MANAGER_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_TOKEN_MANAGER))

#define SHARE_TOKEN_MANAGER_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_TOKEN_MANAGER, ShareTokenManagerClass))

typedef struct _ShareTokenManager        ShareTokenManager;
typedef struct _ShareTokenManagerClass   ShareTokenManagerClass;

struct _ShareTokenManager
{
    ShareManager manager;
};

struct _ShareTokenManagerClass
{
    ShareManagerClass parent_class;
};


GType 
share_token_manager_get_type          (void);

ShareTokenManager * 
share_token_manager_new               (void);

gboolean 
share_token_manager_token_request     (ShareTokenManager *token_manager,
                                       ShareToken *token,
                                       ShareAuthenticationInfo *info,
                                       ShareClient *client);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_TOKEN_MANAGER_H__ */
