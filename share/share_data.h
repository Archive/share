
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_DATA_H__
#define __SHARE_DATA_H__

#include <share/share_types.h>
#include <share/share_channel.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_DATA \
            (share_data_get_type())

#define SHARE_DATA(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_DATA, ShareData))

#define SHARE_DATA_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_DATA, ShareDataClass))

#define SHARE_IS_DATA(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_DATA))

#define SHARE_IS_DATA_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_DATA))

#define SHARE_DATA_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_DATA, ShareDataClass))

typedef struct _ShareDataClass ShareDataClass;

struct _ShareData
{
    ShareObject object;
};

struct _ShareDataClass
{
    ShareObjectClass parent_class;
};


GType     
share_data_get_type             (void);

ShareData *    
share_data_new                  (byte data[]);

ShareData *    
share_data_new_with_length      (byte data[],
                                 gint length);

ShareData *    
share_data_new_with_string      (gchar *string);

ShareData *    
share_data_new_with_object      (GObject *object);

byte *       
share_data_get_data_as_bytes    (ShareData *data);

GObject *  
share_data_get_data_as_object   (ShareData *data);

gchar *  
share_data_get_data_as_string   (ShareData *data);

gint        
share_data_get_length           (ShareData *data);

gchar *  
share_data_get_sender_name      (ShareData *data);

gint        
share_data_get_priority         (ShareData *data);
 
void        
share_data_set_priority         (ShareData *data,
                                 gint priority);
 
ShareChannel *  
share_data_get_channel          (ShareData *data);
 
void        
share_data_set_channel          (ShareData *data,
                                 ShareChannel *channel);
 
void        
share_data_set_sender_name      (ShareData *data,
                                 gchar *sender_name);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_DATA_H__ */
