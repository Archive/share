
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The Manageable object.
 *
 * This is the parent for all manageable resources (ByteArrays, Channels,
 * Sessions and Tokens).
 */

#include <share/share_manageable.h>

static void share_manageable_class_init     (ShareManageableClass *class);
static void share_manageable_instance_init  (ShareManageable      *instance);


GType      
share_manageable_get_type(void)
{
    static GType manageable_type = 0;

    if (!manageable_type) {
        static const GTypeInfo manageable_info = {
            sizeof(ShareManageableClass),
            NULL,			/* base_init */
            NULL,			/* base_finalize */
            (GClassInitFunc) share_manageable_class_init,
            NULL,			/* class_finalize */
            NULL,			/* class_data */
            sizeof(ShareManageable),
            0,				/* n_preallocs */
            (GInstanceInitFunc) share_manageable_instance_init,
        };

        manageable_type = g_type_register_static(SHARE_TYPE_OBJECT,
                                                 "ShareManageable",
                                                 &manageable_info, 0);
    }

    return(manageable_type);
}


static void 
share_manageable_class_init(ShareManageableClass *class)
{
    printf("share_manageable_class_init called.\n");
}


static void 
share_manageable_instance_init(ShareManageable *instance)
{
    printf("share_manageable_instance_init called.\n");
}


ShareManageable * 
share_manageable_new(void)
{
    printf("share_manageable_new called.\n");
}


/**
 * share_manageable_get_name:
 * @manageable: the Manageable object.
 *
 * Get the name of this object.
 *
 * Returns: the name of this object.
 */

gchar * 
share_manageable_get_name(ShareManageable *manageable)
{
    printf("share_manageable_get_name called.\n");
}


/**
 * share_manageable_get_session_name:
 * @manageable: the Manageable object.
 *
 * Get the name of the Session that this manageable object belongs to.
 *
 * Returns: the name of the Session that this manageable object belongs to.
 */

gchar * 
share_manageable_get_session_name(ShareManageable *manageable)
{
    printf("share_manageable_get_session_name called.\n");
}


/**
 * share_manageable_enable_manager_events:
 * @manageable: the Manageable object.
 * @manager: the manager whose event mask is being changed.
 * @event_mask: the mask of events to be enabled.
 *
 * Enable certain events for the manager (if any) associated with this
 * manageable object.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this ByteArray doesn't 
 *       exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if this Token doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_MANAGER_EXCEPTION if this Manager doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_manageable_enable_manager_events(ShareManageable *manageable,
                                       ShareManager *manager,
                                       gint event_mask)
{
    printf("share_manageable_enable_manager_events called.\n");
}


/**
 * share_manageable_disable_manager_events:
 * @manageable: the Manageable object.
 * @manager: the manager whose event mask is being changed.
 * @event_mask: the mask of events to be disabled.
 *
 * Disable certain events for the manager (if any) associated with this
 * manageable object.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this ByteArray doesn't 
 *       exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if this Token doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_MANAGER_EXCEPTION if this Manager doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_manageable_disable_manager_events(ShareManageable *manageable,
                                        ShareManager *manager,
                                        gint event_mask)
{
    printf("share_manageable_disable_manager_events called.\n");
}


/**
 * share_manageable_expel:
 * @manageable: the Manageable object.
 * @clients: the list of Clients to be expelled from this object.
 *
 * Expel Clients from this Manageable object.
 *
 * This method should only be called by the manager for this object.
 * An indication is delivered to each listener of this Manageable object,
 * for each Client expelled.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this ByteArray doesn't 
 *       exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if this Token doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *       permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_manageable_expel(ShareManageable *manageable,
                       ShareClient *clients[])
{
    printf("share_manageable_expel called.\n");
}


/**
 * share_manageable_invite:
 * @manageable: the Manageable object.
 * @clients: the list of Clients to be invited to join this object.
 *
 * Invite Clients to join this Manageable object.
 *
 * This method should only be called by the manager for this object.
 * An indication is delivered to each listener of this Manageable object,
 * for each Client invited.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this ByteArray doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if this Token doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *       permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_manageable_invite(ShareManageable *manageable,
                        ShareClient *clients[])
{
    printf("share_manageable_invite called.\n");
}


/**
 * share_manageable_destroy:
 * @manageable: the Manageable object.
 * @client: the Client wishing to destroy this object.
 *
 * Destroy this Manageable object.
 *
 * An indication is delivered to each listener of this Manageable object,
 * that it has been destroyed. If this is for a ByteArray, Channel or Token
 * in a managed Session, then the Client is authenticated to determine if
 * it is permitted to do this operation.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this ByteArray doesn't 
 *       exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if this Token doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *       permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_manageable_destroy(ShareManageable *manageable,
                         ShareClient *client)
{
    printf("share_manageable_destroy called.\n");
}


/**
 * share_manageable_is_managed:
 * @manageable: the Manageable object.
 * @managed: return whether this managed object has a manager associated 
 *          with it.
 *
 * Test whether this managed object actually has a manager associated with it.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this ByteArray doesn't 
 *       exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if this Token doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operati
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_manageable_is_managed(ShareManageable *manageable,
                            gboolean *managed)
{
    printf("share_manageable_is_managed called.\n");
}


/**
 * share_manageable_join:
 * @manageable: the Manageable object.
 * @client: the Client wishing to join this Manageable object.
 *
 * Join a Client to this Manageable object.
 *
 * If this is a managed object, then the Client is authenticated to
 * determine if it is permitted to do this operation.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this ByteArray doesn't 
 *       exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if this Token doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *	 permission for this operation.</listitem>
 *   <listitem>SHARE_NAME_IN_USE_EXCEPTION if a Client with this name is already
 *       joined to this Manageable object.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *	 in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_manageable_join(ShareManageable *manageable,
                      ShareClient *client)
{
    printf("share_manageable_join called.\n");
}


/**
 * share_manageable_leave:
 * @manageable: the Manageable object.
 * @client: the Client in question.
 *
 * Removes a Client from this Manageable object. This Client will no
 * longer be known to this object. Listeners of this object will be
 * sent an indication when this happens.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this ByteArray doesn't 
 *       exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if this Token doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_manageable_leave(ShareManageable *manageable,
                       ShareClient *client)
{
    printf("share_manageable_leave called.\n");
}


/**
 * share_manageable_list_client_names:
 * @manageable: the Manageable object.
 * @names: return a sorted array of names of Clients currently joined 
 *        to this object.
 *
 * List the names of the Clients who are joined to this Manageable object.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_BYTE_ARRAY_EXCEPTION if this ByteArray doesn't 
 *       exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if this Token doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this operation
 *       in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_manageable_list_client_names(ShareManageable *manageable,
                                   gchar *names[])
{
    printf("share_manageable_list_client_names called.\n");
}
