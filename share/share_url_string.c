
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Share URL string parsing object. A Share URL is of the form:
 *
 * <PRE>
 *	share://&#60host&#62:&#60port&#62/&#60connection type&#62/&#60object type&#62/&#60object name&#62
 *
 * where <connection type> is the connection (or implementation) type,
 * and where valid object types are "Session" and "Client".
 *
 * So for example:
 *
 *   "share://stard.eng.sun.com:3355/dbus/Session/chatSession"
 *
 *   "share://stard.eng.sun.com:4386/dbus/Client/fredClient"
 * </PRE>
 *
 * Convenience methods are provided to create Session and Client URL strings.
 */

#include <share/share_url_string.h>

static void share_url_string_class_init     (ShareUrlStringClass *class);
static void share_url_string_instance_init  (ShareUrlString *instance);


GType       
share_url_string_get_type(void)
{
    printf("share_url_string_get_type called.\n");
}


static void 
share_url_string_class_init(ShareUrlStringClass *class)
{
    printf("share_url_string_class_init called.\n");
}


static void 
share_url_string_instance_init(ShareUrlString *instance)
{
    printf("share_url_string_instance_init called.\n");
}


/**
 * share_url_string_new:
 * @url: the Share URL string to be broken down.
 *
 * Constructor for the ShareUrlString object. It takes a Share URL string and 
 * divides it up into its component parts.
 *
 * Returns: a ShareUrlString object.
 */

static ShareUrlString * 
share_url_string_new(gchar *url)
{
    printf("share_url_string_new called.\n");
}


/**
 * share_url_string_new_session_url:
 * @host_name: the host name for the server for this Session.
 * @port: the port number the server is running on.
 * @connection_type: the connection (implementation) type of this Session.
 * @session_name: the name of this Session.
 *
 * Create a new Share Session URL given it's component parts. This URL can 
 * then be used in conjunction with share_session_factory_create_session()
 *
 * Returns: a ShareUrlString object.
 */

ShareUrlString * 
share_url_string_new_session_url(gchar *host_name,
                                 gint port,
                                 gchar *connection_type,
                                 gchar *session_name)
{
    printf("share_url_string_new_session_url called.\n");
}


/**
 * share_url_string_new_client_url:
 * @host_name: the host name for the server for this Client.
 *        In other words, the name of the machine that is running the Registry
 *        where this special Share Client will be bound.
 * @port: the port number the Client is running on. This is a port on 
 *        the local machine; not the port number of the Registry.
 * @connection_type: the connection (implementation) type of this Client.
 * @client_name: the name of this Client.
 *
 * Create a new Share Client URL given it's component parts. This URL can 
 * then be used in conjunction with share_client_factory_create_client()
 *
 * Returns: a ShareUrlString object.
 */

ShareUrlString * 
share_url_string_new_client_url(gchar *host_name,
                                gint port, 
                                gchar *connection_type, 
                                gchar *client_name)
{
    printf("share_url_string_new_client_url called.\n");
}


/**
 * share_url_string_get_host_address:
 * @url_string: the shared URL string.
 *
 * Get the host IP address for the server for this object.
 *
 * Returns: the host IP address of the server for this object.
 */

gchar * 
share_url_string_get_host_address(ShareUrlString *url_string)
{
    printf("share_url_string_get_host_address called.\n");
}


/**
 * share_url_string_get_host_name:
 * @url_string: the shared URL string.
 *
 * Get the host name for the server for this object.
 *
 * Returns: the host name of the server for this object.
 */

gchar * 
share_url_string_get_host_name(ShareUrlString *url_string)
{
    printf("share_url_string_get_host_name called.\n");
}


/**
 * share_url_string_get_protocol:
 * @url_string: the shared URL string.
 *
 * Get the protocol portion of the given URL string.
 *
 * Returns: the port portion of the given URL string.
 */

gchar * 
share_url_string_get_protocol(ShareUrlString *url_string)
{
    printf("share_url_string_get_protocol called.\n");
}


/**
 * share_url_string_get_port:
 * @url_string: the shared URL string.
 *
 * Get the port number being used by the server for this object.
 *
 * Returns: the port number being used by the server for this object.
 */

gint    
share_url_string_get_port(ShareUrlString *url_string)
{
    printf("share_url_string_get_port called.\n");
}


/**
 * share_url_string_get_connection_type:
 * @url_string: the shared URL string.
 *
 * Get the connection type of this Session/Client.
 *
 * Returns: the connection type of this Session/Client.
 */

gchar * 
share_url_string_get_connection_type(ShareUrlString *url_string)
{
    printf("share_url_string_get_connection_type called.\n");
}


/**
 * share_url_string_get_object_type:
 * @url_string: the shared URL string.
 *
 * Get the type of this object (Session or Client).
 *
 * Returns: the type of this object (Session or client).
 */

gchar * 
share_url_string_get_object_type(ShareUrlString *url_string)
{
    printf("share_url_string_get_object_type called.\n");
}


/**
 * share_url_string_get_object_name:
 * @url_string: the shared URL string.
 *
 * Get the name of this object.
 *
 * Returns: the name of this object.
 */

gchar * 
share_url_string_get_object_name(ShareUrlString *url_string)
{
    printf("share_url_string_get_object_name called.\n");
}


/**
 * share_url_string_is_valid:
 * @url_string: the shared URL string.
 *
 * Return the validity of this ShareUrlString.
 *
 * Returns: an indication of whether this ShareUrlString is valid.
 */

gboolean 
share_url_string_is_valid(ShareUrlString *url_string)
{
    printf("share_url_string_is_valid called.\n");
}


/**
 * share_url_string_equals:
 * @url_string: the shared URL string.
 * @object: the object to compare this #ShareUrlString against.
 *
 * Compares this ShareUrlString to the specified object.
 * The result is %TRUE if and only if the argument is not
 * %NULL and is a #ShareUrlString object that
 * represents the same Share URL as this object.
 *
 * Returns: %TRUE if the #ShareUrlString are equal; %FALSE otherwise.
 */

gboolean 
share_url_string_equals(ShareUrlString *url_string,
                        GObject *object)
{
    printf("share_url_string_equals called.\n");
}


/**
 * share_url_string_to_string:
 * @url_string: the shared URL string.
 *
 * Return this ShareUrlString object as a Share string URL.
 *
 * Returns: this ShareUrlString object as a Share String URL.
 */

gchar *
share_url_string_to_string(ShareUrlString *url_string)
{
    printf("share_url_string_to_string called.\n");
}
