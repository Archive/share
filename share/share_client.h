
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_CLIENT_H__
#define __SHARE_CLIENT_H__

#include <share/share_types.h>
#include <share/share_authentication_info.h>
#include <share/share_client_event.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_CLIENT \
            (share_client_get_type())

#define SHARE_CLIENT(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_CLIENT, ShareClient))

#define SHARE_CLIENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_CLIENT, ShareClientClass))

#define SHARE_IS_CLIENT(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_CLIENT))

#define SHARE_IS_CLIENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_CLIENT))

#define SHARE_CLIENT_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_CLIENT, ShareClientClass))

typedef struct _ShareClientClass      ShareClientClass;

struct _ShareClient
{
    ShareObject object;
};

struct _ShareClientClass
{
    ShareObjectClass parent_class;

    void (* share_byte_array_expelled) (ShareClient *client,
                                        ShareClientEvent *event);

    void (* share_byte_array_invited)  (ShareClient *client,
                                        ShareClientEvent *event);

    void (* share_channel_expelled)    (ShareClient *client,
                                        ShareClientEvent *event);

    void (* share_channel_invited)     (ShareClient *client,
                                        ShareClientEvent *event);

    void (* share_session_expelled)    (ShareClient *client,
                                        ShareClientEvent *event);

    void (* share_session_invited)     (ShareClient *client,
                                        ShareClientEvent *event);

    void (* share_token_expelled)      (ShareClient *client,
                                        ShareClientEvent *event);

    void (* share_token_given)         (ShareClient *client,
                                        ShareClientEvent *event);

    void (* share_token_invited)       (ShareClient *client,
                                        ShareClientEvent *event);

};

GType    
share_client_get_type        (void);

ShareClient * 
share_client_new            (gchar *name);

GObject * 
share_client_authenticate   (ShareClient *client,
                             ShareAuthenticationInfo *info);

gchar * 
share_client_get_name       (ShareClient *client);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_CLIENT_H__ */
