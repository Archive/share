
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The Token object.
 */

#include <share/share_token.h>
#include <share/share_token_event.h>
#include <share/share_marshal.h>

enum {
    SHARE_TOKEN_EXPELLED_SIGNAL,
    SHARE_TOKEN_GIVEN_SIGNAL,
    SHARE_TOKEN_GRABBED_SIGNAL,
    SHARE_TOKEN_INVITED_SIGNAL,
    SHARE_TOKEN_JOINED_SIGNAL,
    SHARE_TOKEN_LEFT_SIGNAL,
    SHARE_TOKEN_RELEASED_SIGNAL,
    SHARE_TOKEN_REQUESTED_SIGNAL,
    SHARE_TOKEN_LAST_SIGNAL
};

static void share_token_class_init     (ShareTokenClass *class);
static void share_token_instance_init  (ShareToken *instance);

static guint token_signals[SHARE_TOKEN_LAST_SIGNAL] = { 0 };
static ShareManageableClass *parent_class = NULL;


GType    
share_token_get_type(void)
{
    static GType token_type = 0;

    if (!token_type) {
	static const GTypeInfo token_info = {
            sizeof(ShareTokenClass),
            NULL,			/* base_init */
            NULL,			/* base_finalize */
            (GClassInitFunc) share_token_class_init,
            NULL,			/* class_finalize */
            NULL,			/* class_data */
            sizeof(ShareToken),
            0,				/* n_preallocs */
            (GInstanceInitFunc) share_token_instance_init,
	};

	token_type = g_type_register_static(SHARE_TYPE_MANAGEABLE,
                                            "ShareToken",
                                            &token_info, 0);
    }

    return(token_type);
}


static void 
share_token_class_init(ShareTokenClass *class)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS(class);

    parent_class = g_type_class_peek_parent(class);

    token_signals[SHARE_TOKEN_EXPELLED_SIGNAL] =
	g_signal_new("share_token_expelled",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareTokenClass,
                            share_token_expelled),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_TOKEN,
            SHARE_TYPE_TOKEN_EVENT);

    token_signals[SHARE_TOKEN_GIVEN_SIGNAL] =
		g_signal_new("share_token_given",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareTokenClass,
                            share_token_given),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_TOKEN,
            SHARE_TYPE_TOKEN_EVENT);

    token_signals[SHARE_TOKEN_GRABBED_SIGNAL] =
		g_signal_new("share_token_grabbed",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareTokenClass,
                            share_token_grabbed),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_TOKEN,
            SHARE_TYPE_TOKEN_EVENT);

    token_signals[SHARE_TOKEN_INVITED_SIGNAL] =
		g_signal_new("share_token_invited",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareTokenClass,
                            share_token_invited),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_TOKEN,
            SHARE_TYPE_TOKEN_EVENT);

    token_signals[SHARE_TOKEN_JOINED_SIGNAL] =
		g_signal_new("share_token_joined",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareTokenClass,
                            share_token_joined),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_TOKEN,
            SHARE_TYPE_TOKEN_EVENT);

    token_signals[SHARE_TOKEN_LEFT_SIGNAL] =
		g_signal_new("share_token_left",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareTokenClass,
                            share_token_left),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_TOKEN,
            SHARE_TYPE_TOKEN_EVENT);

    token_signals[SHARE_TOKEN_RELEASED_SIGNAL] =
		g_signal_new("share_token_released",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareTokenClass,
                            share_token_released),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_TOKEN,
            SHARE_TYPE_TOKEN_EVENT);

    token_signals[SHARE_TOKEN_REQUESTED_SIGNAL] =
		g_signal_new("share_token_requested",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareTokenClass,
                            share_token_requested),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_TOKEN,
            SHARE_TYPE_TOKEN_EVENT);
}


static void 
share_token_instance_init(ShareToken *instance)
{
    printf("share_token_instance_init called.\n");
}


ShareToken * 
share_token_new(void)
{
    ShareByteArray *token;

    token = g_object_new(share_token_get_type(), NULL);

    return(token);
}


/**
 * share_token_give:
 * @token: the Token.
 * @client: the Client giving this Token.
 * @receiving_client_name: the name of the Client to receive the Token.
 * @token_status: return the status of the token.
 *
 * Used by a Client to surrender a Token to another Client. It will fail if
 * the requestor has not grabbed the specified Token within a certain period
 * of time.
 *
 * A GIVEN TokenEvent is delivered to the Token listener of the
 * Client that this Client would like to give the Token to.
 *
 * The giving Client must be either exclusively grabbing the Token or the
 * only Client non-exclusively inhibiting this Token for the give
 * operation to be permissible.
 *
 * A Token being passed between two Clients and whose possession is not yet
 * resolved will appear to any Client requesting test to be giving
 * by some other Client and not held by the requestor. grab will fail
 * during this interval, even if requested by one of the two Clients involved.
 * release requested by the given will succeed, with the result that
 * the Token is released if the offer to the recipient is ultimately rejected.
 * release by the recipient will have no effect, just like the
 * attempted release of any other Token that the requester does not yet
 * possess. During the interval that a Token is being passed any
 * request indications that are generated will be delivered to both
 * Clients involved.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if this Token doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if the giving or receiving Client 
 *       doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client does not have
 *       permission to perform this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_token_give(ShareToken *token,
                 ShareClient *client,
                 gchar *receiving_client_name,
                 gint *token_status)
{
    printf("share_token_give called.\n");
}


/**
 * share_token_grab:
 * @token: the Token.
 * @client: the Client grabbing/inhibiting this Token.
 * @exclusive: indicates whether the grab should be exclusive.
 * @token_status: return the status of the token.
 *
 * Used by a Client to take exclusive (grab) or non-exclusive (inhibit)
 * control of a specific Token.
 *
 * With an exclusive grab, the grab will succeed if the requestor is the
 * sole inhibitor of the Token.
 *
 * The non-exclusive grab is used to take non-exclusive control of a specific 
 * Token. It is used to prevent someone else from exclusively grabbing the
 * Token. Several Clients could inhibit a Token at the same time. This
 * operation will succeed if the requestor has grabbed the Token. The result
 * will be that the Token is no longer grabbed exclusively and is inhibited
 * instead. Therefore it may be inhibited by other Clients too.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if this Token doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if the giving or receiving Client 
 *       doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client does not have
 *       permission to perform this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *       operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_token_grab(ShareToken *token,
                 ShareClient *client,
                 gboolean exclusive,
                 gint *token_status)
{
    printf("share_token_grab called.\n");
}


/**
 * share_token_list_holder_names:
 * @token: the Token.
 * @names: return a sorted array of names of Clients currently holding 
 *        this Token. This array will be of zero length if there are no 
 *        Clients currently holding this Token.
 *
 * List the names of the Clients who are currently holding (grabbing or
 * inhibiting) this Token. This method can be used in conjunction with the
 * share_token_test() routine to determine if the token is being grabbed or 
 * inhibited.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if this Token doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 *
 * See:         share_token_test
 */

ShareStatus 
share_token_list_holder_names(ShareToken *token,
                              gchar *names[])
{
    printf("share_token_list_holder_names called.\n");
}


/**
 * share_token_request:
 * @token: the Token.
 * @client: the Client requesting this Token.
 * @token_status: return the status of the token.
 *
 * Used by a Client to request a Token from the current possessor(s) of the
 * Token. A Token may be inhibited by several Clients, or it may be grabbed
 * by one Client. In any case, a REQUESTED TokenEvent is delivered
 * to all the Token listeners listening to this Token, who currently possess
 * the Token.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if this Token doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if the giving or receiving Client
 *	 doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client does not have
 *	 permission to perform this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_token_request(ShareToken *token,
                    ShareClient *client,
                    gint *token_status)
{
    printf("share_token_request called.\n");
}


/**
 * share_token_release:
 * @token: the Token.
 * @client: the Client releasing this Token.
 * @token_status: return the status of the token.
 *
 * Used to free up a previously grabbed/inhibited Token.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if this Token doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if the giving or receiving Client
 *       doesn't exist.</listitem>
 *   <listitem>SHARE_CLIENT_NOT_GRABBING_EXCEPTION if this Client is not grabbing 
 *       this Token.</listitem>
 *   <listitem>SHARE_CLIENT_NOT_RELEASED_EXCEPTION if the Token was not released
 *       successfully.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client does not have
 *       permission to perform this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *       operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_token_release(ShareToken *token,
                    ShareClient *client,
                    gint *token_status)
{
    printf("share_token_release called.\n");
}


/**
 * share_token_test:
 * @token: the Token.
 * @token_status: return one of the following:
 * <itemizedlist>
 *   <listitem>NOT_IN_USE : not in use</listitem>
 *   <listitem>GRABBED    : grabbed</listitem>
 *   <listitem>INHIBITED  : inhibited</listitem>
 *   <listitem>GIVING	    : giving</listitem>
 * </itemizedlist>
 *
 * Used to check the status of a Token.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_TOKEN_EXCEPTION if this Token doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *       operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_token_test(ShareToken *token,
                 gint *token_status)
{
    printf("share_token_test called.\n");
}
