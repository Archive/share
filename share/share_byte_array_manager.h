
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_BYTE_ARRAY_MANAGER_H__
#define __SHARE_BYTE_ARRAY_MANAGER_H__

#include <share/share_manager.h>
#include <share/share_authentication_info.h>
#include <share/share_client.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_BYTE_ARRAY_MANAGER \
            (share_byte_array_manager_get_type())

#define SHARE_BYTE_ARRAY_MANAGER(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_BYTE_ARRAY_MANAGER, ShareByteArrayManager))

#define SHARE_BYTE_ARRAY_MANAGER_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_BYTE_ARRAY_MANAGER, ShareByteArrayManagerClass))

#define SHARE_IS_BYTE_ARRAY_MANAGER(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_BYTE_ARRAY_MANAGER))

#define SHARE_IS_BYTE_ARRAY_MANAGER_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_BYTE_ARRAY_MANAGER))

#define SHARE_BYTE_ARRAY_MANAGER_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_BYTE_ARRAY_MANAGER, ShareByteArrayManagerClass))

typedef struct _ShareByteArrayManagerClass ShareByteArrayManagerClass;

struct _ShareByteArrayManager
{
    ShareManager manager;
};

struct _ShareByteArrayManagerClass
{
    ShareManagerClass parent_class;
};


GType 
share_byte_array_manager_get_type   (void);

ShareByteArrayManager * 
share_byte_array_manager_new        (void);

gboolean 
share_byte_array_manager_byte_array_request
                                    (ShareByteArrayManager *byte_array_manager,
                                     ShareByteArray *byte_array,
                                     ShareAuthenticationInfo *info,
                                     ShareClient *client);
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_BYTE_ARRAY_MANAGER_H__ */
