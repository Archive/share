
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_SESSION_H__
#define __SHARE_SESSION_H__

#include <share/share_manageable.h>
#include <share/share_client.h>
#include <share/share_byte_array.h>
#include <share/share_byte_array_manager.h>
#include <share/share_channel.h>
#include <share/share_channel_manager.h>
#include <share/share_session_event.h>
#include <share/share_session_manager.h>
#include <share/share_token.h>
#include <share/share_token_manager.h>
#include <share/share_url_string.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_SESSION \
            (share_session_get_type())

#define SHARE_SESSION(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_SESSION, ShareSession))

#define SHARE_SESSION_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_SESSION, ShareSessionClass))

#define SHARE_IS_SESSION(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_SESSION))

#define SHARE_IS_SESSION_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_SESSION))

#define SHARE_SESSION_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_SESSION, ShareSessiontClass))

typedef struct _ShareSessionClass ShareSessionClass;

struct _ShareSession
{
    ShareManageable manageable;
};

struct _ShareSessionClass
{
    ShareManageableClass parent_class;

    void (* share_byte_array_created)    (ShareSession *session,
                                          ShareSessionEvent *event);

    void (* share_byte_array_destroyed)  (ShareSession *session,
                                          ShareSessionEvent *event);

    void (* share_channel_created)       (ShareSession *session,
                                          ShareSessionEvent *event);

    void (* share_channel_destroyed)     (ShareSession *session,
                                          ShareSessionEvent *event);

    void (* share_session_destroyed)     (ShareSession *session,
                                          ShareSessionEvent *event);

    void (* share_session_expelled)      (ShareSession *session,
                                          ShareSessionEvent *event);

    void (* share_session_invited)       (ShareSession *session,
                                          ShareSessionEvent *event);

    void (* share_session_joined)        (ShareSession *session,
                                          ShareSessionEvent *event);

    void (* share_session_left)          (ShareSession *session,
                                          ShareSessionEvent *event);

    void (* share_token_created)         (ShareSession *session,
                                          ShareSessionEvent *event);

    void (* share_token_destroyed)       (ShareSession *session,
                                          ShareSessionEvent *event);

};


GType    
share_session_get_type                       (void);

ShareSession *
share_session_factory_new_session            (ShareClient *client,
                                              ShareUrlString *url_string,
                                              gboolean auto_join);

ShareSession *
share_session_factory_new_session_with_manager(ShareClient *client,
                                               ShareUrlString *url_string,
                                               gboolean auto_join,
                                               ShareSessionManager *manager);

ShareStatus 
share_session_create_byte_array              (ShareSession *session,
                                              ShareClient *client,
                                              gchar *byte_array_name,
                                              gboolean auto_join,
                                              ShareByteArray *byte_array);

ShareStatus 
share_session_create_byte_array_with_manager (ShareSession *session,
                                              ShareClient *client,
                                              gchar *byte_array_name,
                                              gboolean auto_join,
                                              ShareByteArrayManager *manager,
                                              ShareByteArray *byte_array);

ShareStatus 
share_session_create_channel                 (ShareSession *session,
                                              ShareClient *client,
                                              gchar *channel_name,
                                              gboolean reliable,
                                              gboolean ordered,
                                              gboolean auto_join,
                                              ShareChannel *channel);

ShareStatus 
share_session_create_channel_with_manager    (ShareSession *session,
                                              ShareClient *client,
                                              gchar *channel_name,
                                              gboolean reliable,
                                              gboolean ordered,
                                              gboolean auto_join,
                                              ShareChannelManager *manager,
                                              ShareChannel *channel);
 
ShareStatus 
share_session_create_token                   (ShareSession *session,
                                              ShareClient *client,
                                              gchar *token_name,
                                              gboolean auto_join,
                                              ShareToken *token);
 
ShareStatus 
share_session_create_token_with_manager      (ShareSession *session,
                                              ShareClient *client,
                                              gchar *token_name,
                                              gboolean auto_join,
                                              ShareTokenManager *manager,
                                              ShareToken *token);
 
ShareStatus 
share_session_byte_array_exists              (ShareSession *session,
                                              gchar *byte_array_name,
                                              gboolean *exists);
 
ShareStatus 
share_session_channel_exists                 (ShareSession *session,
                                              gchar *channel_name,
                                              gboolean *exists);

ShareStatus 
share_session_token_exists                   (ShareSession *session,
                                              gchar *token_name,
                                              gboolean *exists);

ShareStatus 
share_session_byte_array_managed             (ShareSession *session,
                                              gchar *byte_array_name,
                                              gboolean *managed);

ShareStatus 
share_session_channel_managed                (ShareSession *session,
                                              gchar *channel_name,
                                              gboolean *managed);

ShareStatus 
share_session_token_managed                  (ShareSession *session,
                                              gchar *token_name,
                                              gboolean *managed);

ShareStatus 
share_session_get_byte_arrays_joined         (ShareSession *session,
                                              ShareClient *client,
                                              ShareByteArray *byte_arrays[]);

ShareStatus 
share_session_get_channels_joined            (ShareSession *session,
                                              ShareClient *client,
                                              ShareChannel *channels[]);

ShareStatus 
share_session_get_tokens_joined              (ShareSession *session,
                                              ShareClient *client,
                                              ShareToken *tokens[]);

ShareStatus 
share_session_list_byte_array_names          (ShareSession *session,
                                              gchar *names[]);

ShareStatus 
share_session_list_channel_names             (ShareSession *session,
                                              gchar *names[]);

ShareStatus 
share_session_list_token_names               (ShareSession *session,
                                              gchar *names[]);

ShareUrlString * 
share_session_get_url                        (ShareSession *session);

ShareStatus 
share_session_close                          (ShareSession *session,
                                              gboolean close_connection);

#ifdef __cplusplus
}
#endif /* __cplusplus */
 
#endif /* __SHARE_SESSION_H__ */
