
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The Session event. Session events are created for the following actions:
 * <itemizedlist>
 *   <listitem>when a ByteArray has been created.</listitem>
 *   <listitem>when a ByteArray has been destroyed.</listitem>
 *   <listitem>when a Channel has been created.</listitem>
 *   <listitem>when a Channel has been destroyed.</listitem>
 *   <listitem>when a Token has been created.</listitem>
 *   <listitem>when a Token has been destroyed.</listitem>
 *   <listitem>when a Client has joined a Session.</listitem>
 *   <listitem>when a Client has left a Session.</listitem>
 *   <listitem>when a Client has been invited to join a Session.</listitem>
 *   <listitem>when a Client has been expelled from a Session.</listitem>
 *   <listitem>when a Session has been destroyed.</listitem>
 * </itemizedlist>
 */

#include <share/share_session_event.h>

static void share_session_event_class_init     (ShareSessionEventClass *class);
static void share_session_event_instance_init  (ShareSessionEvent      *instance);


GType 
share_session_event_get_type(void)
{
    printf("share_session_event_get_type called.\n");
}


static void 
share_session_event_class_init(ShareSessionEventClass *class)
{
    printf("share_session_event_class_init called.\n");
}


static void 
share_session_event_instance_init(ShareSessionEvent *instance)
{
    printf("share_session_event_instance_init called.\n");
}


/**
 * share_session_event_new:
 * @session: the session in question.
 * @client_name: the name of the client.
 * @resource_name: the name of the resource within the session that the 
 *        event affects.
 * @type: the type of event.
 *
 * Constructor for the SessionEvent class. A new session event is generated
 * for a client action within the given session.
 *
 * Returns: a ShareSessionEvent object.
 */

ShareSessionEvent * 
share_session_event_new(ShareSession *session,
                        gchar *client_name,
                        ShareUrlString *resource_name,
                        gint type)
{
    printf("share_session_event_new called.\n");
}


/**
 * share_session_event_get_client_name:
 * @event: the Session event.
 *
 * Get the name of the Client that generated this event.
 *
 * Returns: the name of the Client that generated this event.
 */

gchar * 
share_session_event_get_client_name(ShareSessionEvent *event)
{
    printf("share_session_event_get_client_name called.\n");
}


/**
 * share_session_event_get_resource_name:
 * @event: the Session event.
 *
 * Get the name of the resource for this event. The resource will be the
 * ByteArray, Channel or Token that the been created or destroyed, or the
 * Session that the Client has just joined, left, been invited to join or
 * expelled from.
 *
 * Returns: the name of the resource for this event.
 */

gchar * 
share_session_event_get_resource_name(ShareSessionEvent *event)
{
    printf("share_session_event_get_resource_name called.\n");
}


/**
 * share_session_event_get_session:
 * @event: the Session event.
 *
 * Get the Session associated with this event.
 *
 * Returns: the Session associated with this event.
 */

ShareSession * 
share_session_event_get_session(ShareSessionEvent *event)
{
    printf("share_session_event_get_session called.\n");
}


/**
 * share_session_event_to_string:
 * @event: the Session event.
 *
 * Return a short description of this Session event.
 *
 * Returns: a String containing a description of this Session event.
 */

gchar * 
share_session_event_to_string(ShareSessionEvent *event)
{
    printf("share_session_event_to_string called.\n");
}
