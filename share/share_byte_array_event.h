
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_BYTE_ARRAY_EVENT_H__
#define __SHARE_BYTE_ARRAY_EVENT_H__

#include <share/share_event.h>
#include <share/share_byte_array.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_BYTE_ARRAY_EVENT \
            (share_byte_array_event_get_type())

#define SHARE_BYTE_ARRAY_EVENT(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_BYTE_ARRAY_EVENT, ShareByteArrayEvent))

#define SHARE_BYTE_ARRAY_EVENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_BYTE_ARRAY_EVENT, ShareByteArrayEventClass))

#define SHARE_IS_BYTE_ARRAY_EVENT(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_BYTE_ARRAY_EVENT))

#define SHARE_IS_BYTE_ARRAY_EVENT_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_BYTE_ARRAY_EVENT))

#define SHARE_BYTE_ARRAY_EVENT_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_BYTE_ARRAY_EVENT, ShareByteArrayEventClass))

typedef struct _ShareByteArrayEventClass   ShareByteArrayEventClass;

struct _ShareByteArrayEvent
{
    ShareEvent event;
};

struct _ShareByteArrayEventClass
{
    ShareEventClass parent_class;
};


#ifdef WANTED
/** The ByteArray joined event type. */
static int SHARE_BYTE_ARRAY_JOINED        = 1 << 0;

/** The ByteArray left event type. */
static int SHARE_BYTE_ARRAY_LEFT          = 1 << 1;

/** The ByteArray value changed event type. */
static int SHARE_BYTE_ARRAY_VALUE_CHANGED = 1 << 2;

/** The ByteArray invited event type. */
static int SHARE_BYTE_ARRAY_INVITED       = 1 << 3;

/** The ByteArray expelled event type. */
static int SHARE_BYTE_ARRAY_EXPELLED      = 1 << 4;
#endif /*WANTED*/

GType
share_byte_array_event_get_type          (void);

ShareByteArrayEvent * 
share_byte_array_event_new               (ShareSession *session,
                                          gchar *client_name,
                                          ShareByteArray *byte_array,
                                          gint type);

ShareByteArray *
share_byte_array_event_get_byte_array    (ShareByteArrayEvent * event);

gchar *
share_byte_array_event_get_client_name   (ShareByteArrayEvent * event);

ShareSession * 
share_byte_array_event_get_session       (ShareByteArrayEvent * event);

gchar * 
share_byte_array_event_to_string         (ShareByteArrayEvent * event);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_BYTE_ARRAY_EVENT_H__ */
