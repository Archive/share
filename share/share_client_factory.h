
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_CLIENT_FACTORY_H__
#define __SHARE_CLIENT_FACTORY_H__

#include <share/share_types.h>
#include <share/share_client.h>
#include <share/share_url_string.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_CLIENT_FACTORY \
            (share_client_factory_get_type())

#define SHARE_CLIENT_FACTORY(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_CLIENT_FACTORY, ShareClientFactory))

#define SHARE_CLIENT_FACTORY_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_CLIENT_FACTORY, ShareClientFactoryClass))

#define SHARE_IS_CLIENT_FACTORY(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_CLIENT_FACTORY))

#define SHARE_IS_CLIENT_FACTORY_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_CLIENT_FACTORY))

#define SHARE_CLIENT_FACTORY_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_CLIENT_FACTORY, ShareClientFactoryClass))

typedef struct _ShareClientFactory      ShareClientFactory;
typedef struct _ShareClientFactoryClass ShareClientFactoryClass;

struct _ShareClientFactory
{
    ShareObject object;
};

struct _ShareClientFactoryClass
{
    ShareObjectClass parent_class;
};


GType 
share_client_factory_get_type             (void);

ShareClientFactory * 
share_client_factory_new                  (void);

ShareStatus 
share_client_factory_create_client        (ShareClientFactory *client_factory,
                                           ShareClient *client,
                                           ShareUrlString *url_string);

ShareStatus 
share_client_factory_lookup_client        (ShareClientFactory *client_factory,
                                           ShareUrlString *url_string,
                                           ShareClient *client);

ShareStatus 
share_client_factory_destroy_client       (ShareClientFactory *client_factory,
                                           ShareClient *client,
                                           ShareUrlString *url_string);

ShareStatus 
share_client_factory_client_exists        (ShareClientFactory *client_factory,
                                           ShareUrlString *url_string,
                                           gboolean *exists);

ShareStatus 
share_client_factory_list_clients         (ShareClientFactory *client_factory,
                                           ShareUrlString *clients[]);

ShareStatus 
share_client_factory_list_clients_on_host (ShareClientFactory *client_factory,
                                           gchar *host,
                                           gchar *connection_type,
                                           ShareUrlString *clients[]);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_CLIENT_FACTORY_H__ */
