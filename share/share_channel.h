
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.          See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_CHANNEL_H__
#define __SHARE_CHANNEL_H__

#include <share/share_manageable.h>
#include <share/share_client.h>
#include <share/share_data.h>
#include <share/share_channel_consumer.h>
#include <share/share_channel_event.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_CHANNEL \
            (share_channel_get_type())

#define SHARE_CHANNEL(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_CHANNEL, ShareChannel))

#define SHARE_CHANNEL_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_CHANNEL, ShareChannelClass))

#define SHARE_IS_CHANNEL(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_CHANNEL))

#define SHARE_IS_CHANNEL_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_CHANNEL))

#define SHARE_CHANNEL_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_CHANNEL, ShareChannelClass))

typedef struct _ShareChannelClass ShareChannelClass;

struct _ShareChannel
{
    ShareManageable manageable;
};

struct _ShareChannelClass
{
    ShareManageableClass parent_class;

    void (* share_channel_consumer_added)    (ShareChannel *channel,
                                              ShareChannelEvent *event);

    void (* share_channel_consumer_removed)  (ShareChannel *channel,
                                              ShareChannelEvent *event);

    void (* share_channel_expelled)          (ShareChannel *channel,
                                              ShareChannelEvent *event);

    void (* share_channel_invited)           (ShareChannel *channel,
                                              ShareChannelEvent *event);

    void (* share_channel_joined)            (ShareChannel *channel,
                                              ShareChannelEvent *event);

    void (* share_channel_left)              (ShareChannel *channel,
                                              ShareChannelEvent *event);

};


/** Channel Data top priority value. */
static int SHARE_CHANNEL_TOP_PRIORITY    = 0;

/** Channel Data high priority value. */
static int SHARE_CHANNEL_HIGH_PRIORITY   = 1;

/** Channel Data medium priority value. */
static int SHARE_CHANNEL_MEDIUM_PRIORITY = 2;

/** Channel Data top priority value. */
static int SHARE_CHANNEL_LOW_PRIORITY    = 3;


/** The Channel read-only value. */
static int SHARE_CHANNEL_READ_ONLY       = 0;

/** The Channel write-only value. */
static int SHARE_CHANNEL_WRITE_ONLY      = 1;

/** The Channel read/write value. */
static int SHARE_CHANNEL_READ_WRITE      = 2;

GType 
share_channel_get_type             (void);

ShareChannel * 
share_channel_new                  (void);

ShareStatus 
share_channel_add_consumer         (ShareChannel *channel,
                                    ShareClient *client,
                                    ShareChannelConsumer *consumer);

ShareStatus 
share_channel_remove_consumer      (ShareChannel *channel,
                                    ShareClient *client,
                                    ShareChannelConsumer *consumer);

ShareStatus 
share_channel_list_consumer_names  (ShareChannel *channel,
                                    gchar *names[]);

gboolean   
share_channel_is_reliable          (ShareChannel *channel);

gboolean   
share_channel_is_ordered           (ShareChannel *channel);

ShareStatus 
share_channel_join                 (ShareChannel *channel,
                                    ShareClient *client,
                                    gint mode);

ShareStatus 
share_channel_receive              (ShareChannel *channel,
                                    ShareClient *client,
                                    ShareData *data);

ShareStatus 
share_channel_receive_with_timeout (ShareChannel *channel,
                                    ShareClient *client,
                                    glong timeout,
                                    ShareData *data);

ShareStatus 
share_channel_data_available       (ShareChannel *channel,
                                    ShareClient *client,
                                    gboolean *is_data);

ShareStatus 
share_channel_send_to_all          (ShareChannel *channel,
                                    ShareClient *sending_client,
                                    ShareData *data);

ShareStatus 
share_channel_send_to_others       (ShareChannel *channel,
                                    ShareClient *sending_client,
                                    ShareData *data);

ShareStatus 
share_channel_send_to_client       (ShareChannel *channel,
                                    ShareClient *sending_client,
                                    gchar *receiving_client_name,
                                    ShareData *data);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _SHARE_CHANNEL_H__ */
