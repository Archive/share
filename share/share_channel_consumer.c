
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.          See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The Channel Consumer object.
 * [XXX: this object probably can be removed when API adjusted to use signals].
 */

#include <share/share_channel_consumer.h>

static void share_channel_consumer_class_init	  (ShareChannelConsumerClass *class);
static void share_channel_consumer_instance_init  (ShareChannelConsumer	*instance);


GType 
share_channel_consumer_get_type(void)
{
    printf("share_channel_consumer_get_type called.\n");
}


static void 
share_channel_consumer_class_init(ShareChannelConsumerClass *class)
{
    printf("share_channel_consumer_class_init called.\n");
}


static void 
share_channel_consumer_instance_init(ShareChannelConsumer *instance)
{
    printf("share_channel_consumer_instance_init called.\n");
}


ShareChannelConsumer * 
share_channel_consumer_new(void)
{
    printf("share_channel_consumer_new called.\n");
}


/**
 * share_channel_consumer_data_received:
 * @consumer: the channel consumer.
 * @data: the Data which can be of unlimited size.
 *
 * Called when Data is received for this Client on the given Channel.
 *
 * The Data object received is a copy of the Client Data which this
 * consumer can do with as they require.
 */

void 
share_channel_consumer_data_received(ShareChannelConsumer *consumer,
                                     ShareData *data)
{
    printf("share_channel_consumer_data_received called.\n");
}
