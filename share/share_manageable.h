
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_MANAGEABLE_H__
#define __SHARE_MANAGEABLE_H__

#include <share/share_types.h>
#include <share/share_object.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_MANAGEABLE \
            (share_manageable_get_type())

#define SHARE_MANAGEABLE(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_MANAGEABLE, ShareManageable))

#define SHARE_MANAGEABLE_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_MANAGEABLE, ShareManageableClass))

#define SHARE_IS_MANAGEABLE(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_MANAGEABLE))

#define SHARE_IS_MANAGEABLE_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_MANAGEABLE))

#define SHARE_MANAGEABLE_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_MANAGEABLE, ShareManageableClass))


typedef struct _ShareManageable		  ShareManageable;
typedef struct _ShareManageableClass	  ShareManageableClass;

struct _ShareManageable
{
    ShareObject object;
};

struct _ShareManageableClass
{
    ShareObjectClass parent_class;
};

#include <share/share_client.h>
#include <share/share_manager.h>
#include <share/share_session.h>


GType      
share_manageable_get_type                (void);

ShareManageable * 
share_manageable_new                     (void);

gchar * 
share_manageable_get_name                (ShareManageable *manageable);

gchar * 
share_manageable_get_session_name        (ShareManageable *manageable);

ShareStatus 
share_manageable_enable_manager_events   (ShareManageable *manageable,
                                          ShareManager *manager,
                                          gint event_mask);

ShareStatus 
share_manageable_disable_manager_events  (ShareManageable *manageable,
                                          ShareManager *manager,
                                          gint event_mask);

ShareStatus 
share_manageable_expel                   (ShareManageable *manageable,
                                          ShareClient *clients[]);

ShareStatus 
share_manageable_invite                  (ShareManageable *manageable,
                                          ShareClient *clients[]);

ShareStatus 
share_manageable_destroy                 (ShareManageable *manageable,
                                          ShareClient *client);

ShareStatus 
share_manageable_is_managed              (ShareManageable *manageable,
                                          gboolean *managed);

ShareStatus 
share_manageable_join                    (ShareManageable *manageable,
                                          ShareClient *client);

ShareStatus 
share_manageable_leave                   (ShareManageable *manageable,
                                          ShareClient *client);

ShareStatus 
share_manageable_list_client_names       (ShareManageable *manageable,
                                          gchar *names[]);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_MANAGEABLE_H__ */
