
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#include <share/share_manager.h>

static void share_manager_class_init     (ShareManagerClass *class);
static void share_manager_instance_init  (ShareManager      *instance);


GType    
share_manager_get_type(void)
{
    printf("share_manager_get_type called.\n");
}


static void 
share_manager_class_init(ShareManagerClass *class)
{
    printf("share_manager_class_init called.\n");
}


static void 
share_manager_instance_init(ShareManager *instance)
{
    printf("share_manager_instance_init called.\n");
}


ShareManager * 
share_manager_new(void)
{
    printf("share_manager_new called.\n");
}
