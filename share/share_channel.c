
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.          See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Share Channel object.
 */

#include <share/share_channel.h>
#include <share/share_marshal.h>

enum {
    SHARE_CHANNEL_CONSUMER_ADDED_SIGNAL,
    SHARE_CHANNEL_CONSUMER_REMOVED_SIGNAL,
    SHARE_CHANNEL_EXPELLED_SIGNAL,
    SHARE_CHANNEL_INVITED_SIGNAL,
    SHARE_CHANNEL_JOINED_SIGNAL,
    SHARE_CHANNEL_LEFT_SIGNAL,
    SHARE_CHANNEL_LAST_SIGNAL
};

static void share_channel_class_init	 (ShareChannelClass *class);
static void share_channel_instance_init  (ShareChannel      *instance);


static guint channel_signals[SHARE_CHANNEL_LAST_SIGNAL] = { 0 };
static ShareManageableClass *parent_class = NULL;

GType 
share_channel_get_type(void)
{
    static GType channel_type = 0;

    if (!channel_type) {
	static const GTypeInfo channel_info = {
            sizeof(ShareChannelClass),
            NULL,			/* base_init */
            NULL,			/* base_finalize */
            (GClassInitFunc) share_channel_class_init,
            NULL,			/* class_finalize */
            NULL,			/* class_data */
            sizeof(ShareChannel),
            0,				/* n_preallocs */
            (GInstanceInitFunc) share_channel_instance_init,
	};

	channel_type = g_type_register_static(SHARE_TYPE_MANAGEABLE,
                                              "ShareChannel",
                                              &channel_info, 0);
    }

    return(channel_type);
}


static void 
share_channel_class_init(ShareChannelClass *class)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS(class);

    parent_class = g_type_class_peek_parent(class);

    channel_signals[SHARE_CHANNEL_CONSUMER_ADDED_SIGNAL] =
	g_signal_new("share_channel_consumer_added",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareChannelClass,
                            share_channel_consumer_added),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CHANNEL,
            SHARE_TYPE_CHANNEL_EVENT);

    channel_signals[SHARE_CHANNEL_CONSUMER_REMOVED_SIGNAL] =
		g_signal_new("share_channel_consumer_removed",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareChannelClass,
                            share_channel_consumer_removed),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CHANNEL,
            SHARE_TYPE_CHANNEL_EVENT);

    channel_signals[SHARE_CHANNEL_EXPELLED_SIGNAL] =
		g_signal_new("share_channel_expelled",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareChannelClass,
                            share_channel_expelled),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CHANNEL,
            SHARE_TYPE_CHANNEL_EVENT);

    channel_signals[SHARE_CHANNEL_INVITED_SIGNAL] =
		g_signal_new("share_channel_invited",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareChannelClass,
                            share_channel_invited),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CHANNEL,
            SHARE_TYPE_CHANNEL_EVENT);

    channel_signals[SHARE_CHANNEL_JOINED_SIGNAL] =
		g_signal_new("share_channel_joined",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareChannelClass,
                            share_channel_joined),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CHANNEL,
            SHARE_TYPE_CHANNEL_EVENT);

    channel_signals[SHARE_CHANNEL_LEFT_SIGNAL] =
		g_signal_new("share_channel_left",
            G_TYPE_FROM_CLASS(gobject_class),
            G_SIGNAL_RUN_LAST,
            G_STRUCT_OFFSET(ShareChannelClass,
                            share_channel_left),
            NULL, NULL,
            _share_marshal_VOID__POINTER_POINTER,
            G_TYPE_NONE, 2,
            SHARE_TYPE_CHANNEL,
            SHARE_TYPE_CHANNEL_EVENT);


}


static void 
share_channel_instance_init(ShareChannel *instance)
{
    printf("share_channel_instance_init called.\n");
}


ShareChannel * 
share_channel_new(void)
{
    ShareChannel *channel;

    channel = g_object_new(share_channel_get_type(), NULL);

    return(channel);
}


/**
 * share_channel_add_consumer:
 * @channel: the shared channel.
 * @client: the Client to associate this ChannelConsumer with.
 * @consumer: the new ChannelConsumer for this client.
 *
 * Add a new ChannelConsumer for this Client. The Client's ChannelConsumers
 * gets informed when there is Data available on a Channel.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *       permission for this operation. This would occur if the Client had 
 *       joined the Channel in write-only mode.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_channel_add_consumer(ShareChannel *channel,
                           ShareClient *client,
                           ShareChannelConsumer *consumer)
{
    printf("share_channel_add_consumer called.\n");
}


/**
 * share_channel_remove_consumer:
 * @channel: the shared channel.
 * @client: the Client to associate this ChannelConsumer with.
 * @consumer: the ChannelConsumer to remove from this Client.
 *
 * Remove a ChannelConsumer from this Client for this Channel. The
 * ChannelConsumer will no longer get informed when there is Data available
 * on this channel.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CONSUMER_EXCEPTION if this ChannelConsumer doesn't 
 *       exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *	 permission for this operation. This would occur if the Client had
 *	 joined the Channel in write-only mode.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_channel_remove_consumer(ShareChannel *channel,
                              ShareClient *client,
                              ShareChannelConsumer *consumer)
{
    printf("share_channel_remove_consumer called.\n");
}


/**
 * share_channel_list_consumer_names:
 * @channel: the shared channel.
 * @names: a sorted array of names of Clients currently consuming 
 * this Channel. This array will be of zero length if there are no Clients 
 * currently consuming this Channel.
 *
 * List the names of the Clients who are currently consuming this Channel.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *       operation in the given timeout period.</listitem>
 * </itemizedlist>

ShareStatus 
share_channel_list_consumer_names(ShareChannel *channel,
                                  gchar *names[])
{
    printf("share_channel_list_consumer_names called.\n");
}


/**
 * share_channel_is_reliable:
 * @channel: the shared channel.
 *
 * Test whether this is a reliable channel.
 *
 * Returns: true if this is a reliable channel; false if not.
 */

gboolean   
share_channel_is_reliable(ShareChannel *channel)
{
    printf("share_channel_is_reliable called.\n");
}


/**
 * share_channel_is_ordered:
 * @channel: the shared channel.
 *
 * Test whether this is an ordered channel.
 *
 * Returns: true if this is an ordered channel; false if not.
 */

gboolean   
share_channel_is_ordered(ShareChannel *channel)
{
    printf("share_channel_is_ordered called.\n");
}


/**
 * share_channel_join:
 * @channel: the shared channel.
 * @client: the Client wishing to join this Channel.
 * @mode: the mode which can be one of, READONLY, WRITEONLY or
 * READWRITE.
 *
 * Join a Client to a Channel in the given mode. This is a prerequesite for
 * sending and receiving Data sent over the Channel.
 *
 * If this is a managed Channel, then the Client is authenticated to
 * determine if it is permitted to do this operation.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *	 permission for this operation.</listitem>
 *   <listitem>SHARE_NAME_IN_USE_EXCEPTION if a Client with this name is already
 *       joined to this Manageable object.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_channel_join(ShareChannel *channel,
                   ShareClient *client,
                   gint mode)
{
    printf("share_channel_join called.\n");
}


/**
 * share_channel_receive:
 * @channel: the shared channel.
 * @client: identifies the client wishing to receive data from
 * this channel. This client must already be successfully joined to this
 * channel.
 * @data: the next Data object available on this channel.
 *
 * Used to receive Data from other Clients joined to this Channel.
 *
 * It will block if there is no Data to read. The 
 * share_channel_data_available() method can be used to check if there is 
 * Data that can be received on this channel.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *       permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *       operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_channel_receive(ShareChannel *channel,
                      ShareClient *client,
                      ShareData *data)
{
    printf("share_channel_receive called.\n");
}


/**
 * share_channel_receive_with_timeout:
 * @channel: the shared channel.
 * @client: identifies the client wishing to receive data from
 * this channel. This client must already be successfully joined to this
 * channel.
 * @timeout: the maximum time to wait in milliseconds.
 * @data: the next Data object available on this channel, or null if the
 *        timeout period has expired, and there is currently no Data available.
 *
 * Used to receive Data from other Clients joined to this Channel.
 *
 * If Data is immediately available then it will return with it, else it
 * will wait until the timeout period, specified by the <code>timeout</code>
 * argument in milliseconds, has elapsed. If no Data is available at this
 * time, it will return null. Note that if Data becomes available during the
 * timeout period, this method will be woken up and that Data is immediately
 * returned.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *	 permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_channel_receive_with_timeout(ShareChannel *channel,
                                   ShareClient *client,
                                   glong timeout,
                                   ShareData *data)
{
    printf("share_channel_receive_with_timeout called.\n");
}


/**
 * share_channel_data_available:
 * @channel: the shared channel.
 * @client: identifies the client wishing to check if there is data 
 *        available to receive on this channel. This client must already be
 *        successfully joined to this channel.
 * @is_data: set to true if Data is available; false if not.
 *
 * Tests whether there is Data available to read on this Channel.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *	 permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_channel_data_available(ShareChannel *channel,
                             ShareClient *client,
                             gboolean *is_data)
{
    printf("share_channel_data_available called.\n");
}


/**
 * share_channel_send_to_all:
 * @channel: the shared channel.
 * @sending_client: the Client sending the Data.
 * @data: the Data being sent over this Channel.
 *
 * Used to send Data to all Clients consuming this Channel. If the
 * sender is a consumer of this Channel, then it too will receive the Data.
 *
 * Data from each sender sent at the same priority on the same Channel
 * arrives at a given receiver in the same order it was sent but may have
 * other sender Data interleaved differently.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *	 permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_channel_send_to_all(ShareChannel *channel,
                          ShareClient *sending_client,
                          ShareData *data)
{
    printf("share_channel_send_to_all called.\n");
}


/**
 * share_channel_send_to_others:
 * @channel: the shared channel.
 * @sending_client: the Client sending the Data.
 * @data: the Data being sent over this Channel.
 *
 * Used to send Data to other Clients consuming this Channel. The
 * sender (irrespective of whether it's a consumer of this channel) will
 * not receive the Data.
 *
 * Data from each sender sent at the same priority on the same Channel
 * arrives at a given receiver in the same order it was sent but may have
 * other sender Data interleaved differently.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *       permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *       operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_channel_send_to_others(ShareChannel *channel,
                             ShareClient *sending_client,
                             ShareData *data)
{
    printf("share_channel_send_to_others called.\n");
}


/**
 * share_channel_send_to_client:
 * @channel: the shared channel.
 * @sending_client: the Client sending the Data.
 * @receiving_client_name: the name of the Client receiving the Data.
 * @data: the Data being sent over this Channel.
 *
 * Used to send Data to a single Client consuming this Channel.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>SHARE_RESULT_OKAY if the operation was successful.</listitem>
 *   <listitem>SHARE_CONNECTION_EXCEPTION if a connection error occured.</listitem>
 *   <listitem>SHARE_INVALID_CLIENT_EXCEPTION if the Client is invalid in some 
 *       way.</listitem>
 *   <listitem>SHARE_NO_SUCH_CHANNEL_EXCEPTION if this Channel doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CLIENT_EXCEPTION if this Client doesn't exist.</listitem>
 *   <listitem>SHARE_NO_SUCH_CONSUMER_EXCEPTION if this Client doesn't have a
 *       ChannelConsumer associated with it.</listitem>
 *   <listitem>SHARE_NO_SUCH_SESSION_EXCEPTION if this Session doesn't exist.</listitem>
 *   <listitem>SHARE_PERMISSION_DENIED_EXCEPTION if this Client doesn't have
 *	 permission for this operation.</listitem>
 *   <listitem>SHARE_TIMED_OUT_EXCEPTION if no reply was received for this
 *	 operation in the given timeout period.</listitem>
 * </itemizedlist>
 */

ShareStatus 
share_channel_send_to_client(ShareChannel *channel,
                             ShareClient *sending_client,
                             gchar *receiving_client_name,
                             ShareData *data)
{
    printf("share_channel_send_to_client called.\n");
}
