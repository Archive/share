
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.          See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_REGISTRY_H__
#define __SHARE_REGISTRY_H__

#include <share/share_types.h>
#include <share/share_manager.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_REGISTRY \
            (share_registry_get_type())

#define SHARE_REGISTRY(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_REGISTRY, ShareRegistry))

#define SHARE_REGISTRY_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_REGISTRY, ShareRegistryClass))

#define SHARE_IS_REGISTRY(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_REGISTRY))

#define SHARE_IS_REGISTRY_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_REGISTRY))

#define SHARE_REGISTRY_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_REGISTRY, ShareRegistryClass))

typedef struct _ShareRegistry      ShareRegistry;
typedef struct _ShareRegistryClass ShareRegistryClass;

struct _ShareRegistry
{
    ShareObject object;
};

struct _ShareRegistryClass
{
    ShareObjectClass parent_class;
};


GType    
share_registry_get_type        (void);

ShareRegistry * 
share_registry_new             (gchar *registry_type);

ShareRegistry *
share_registry_new_with_port   (gchar *registry_type,
                                gint port);

ShareRegistry *
share_registry_new_with_manager (gchar *registry_type,
                                 ShareManager *manager);

ShareStatus 
share_registry_start_registry (ShareRegistry *registry);


ShareStatus 
share_registry_stop_registry  (ShareRegistry *registry);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_REGISTRY_H__ */
