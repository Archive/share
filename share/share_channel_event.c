
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The Channel event. Channel events are created for the following actions:
 * <itemizedlist>
 *   <listitem>when a Client has joined a Channel.</listitem>
 *   <listitem>when a Client has left a Channel.</listitem>
 *   <listitem>when a Client has been invited to join a Channel.</listitem>
 *   <listitem>when a Client has been expelled from a Channel.</listitem>
 *   <listitem>when a Client has added a Consumer to the Channel.</listitem>
 *   <listitem>when a Client has removed a Consumer from the Channel.</listitem>
 * </itemizedlist>
 */

#include <share/share_channel_event.h>

static void share_channel_event_class_init     (ShareChannelEventClass *class);
static void share_channel_event_instance_init  (ShareChannelEvent      *instance);


GType 
share_channel_event_get_type(void)
{
    printf("share_channel_event_get_type called.\n");
}


static void 
share_channel_event_class_init(ShareChannelEventClass *class)
{
    printf("share_channel_event_class_init called.\n");
}


static void 
share_channel_event_instance_init(ShareChannelEvent *instance)
{
    printf("share_channel_event_instance_init called.\n");
}


/**
 * share_channel_event_new:
 * @session: the session this channel belongs to.
 * @client_name: the name of the client.
 * @channel: the channel.
 * @type: the type of event.
 *
 * Constructor for the ChannelEvent object. A new channel event is generated
 * for a client action for a specific channel.
 *
 * Returns: a ShareChannelEvent object.
 */

ShareChannelEvent * 
share_channel_event_new(ShareSession *session,
                        gchar *client_name,
                        ShareChannel *channel,
                        gint type)
{
    printf("share_channel_event_new called.\n");
}


/**
 * share_channel_event_get_channel:
 * @event: the Channel event.
 *
 * Get the Channel associated with this event.
 *
 * Returns: the Channel associated with this event.
 */

ShareChannel * 
share_channel_event_get_channel(ShareChannelEvent *event)
{
    printf("share_channel_event_get_channel called.\n");
}


/**
 * share_channel_event_get_client_name:
 * @event: the Channel event.
 *
 * Get the name of the Client that generated this event.
 *
 * Returns: the name of the Client that generated this event.
 */

gchar * 
share_channel_event_get_client_name(ShareChannelEvent *event)
{
    printf("share_channel_event_get_client_name called.\n");
}


/**
 * share_channel_event_get_session:
 * @event: the Channel event.
 *
 * Get the Session associated with this event.
 *
 * Returns: the Session associated with this event.
 */

ShareSession * 
share_channel_event_get_session(ShareChannelEvent *event)
{
    printf("share_channel_event_get_session called.\n");
}


/**
 * share_channel_event_to_string:
 * @event: the Channel event.
 *
 * Return a short description of this Channel event.
 *
 * Returns: a string containing a description of this Channel event.
 */

gchar * 
share_channel_event_to_string(ShareChannelEvent *event)
{
    printf("share_channel_event_to_string called.\n");
}
