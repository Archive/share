
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SHARE_SESSION_FACTORY_H__
#define __SHARE_SESSION_FACTORY_H__

#include <share/share_object.h>
#include <share/share_client.h>
#include <share/share_session.h>
#include <share/share_url_string.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SHARE_TYPE_SESSION_FACTORY \
            (share_session_factory_get_type())

#define SHARE_SESSION_FACTORY(obj) \
            (G_TYPE_CHECK_INSTANCE_CAST((obj), SHARE_TYPE_SESSION_FACTORY, ShareSessionFactory))

#define SHARE_SESSION_FACTORY_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_CAST((klass), SHARE_TYPE_SESSION_FACTORY, ShareSessionFactoryClass))

#define SHARE_IS_SESSION_FACTORY(obj) \
            (G_TYPE_CHECK_INSTANCE_TYPE((obj), SHARE_TYPE_SESSION_FACTORY))

#define SHARE_IS_SESSION_FACTORY_CLASS(klass) \
            (G_TYPE_CHECK_CLASS_TYPE((klass), SHARE_TYPE_SESSION_FACTORY))

#define SHARE_SESSION_FACTORY_GET_CLASS(obj) \
            (G_TYPE_INSTANCE_GET_CLASS((obj), SHARE_TYPE_SESSION_FACTORY, ShareSessionFactoryClass))

typedef struct _ShareSessionFactory      ShareSessionFactory;
typedef struct _ShareSessionFactoryClass ShareSessionFactoryClass;

struct _ShareSessionFactory
{
    ShareObject object;
};

struct _ShareSessionFactoryClass
{
    ShareObjectClass parent_class;
};


GType    
share_session_factory_get_type         (void);

ShareStatus 
share_session_factory_destroy_session (ShareSessionFactory *session_factory,
                                       ShareClient *client,
                                       ShareUrlString *url_string);

ShareStatus 
share_session_factory_session_exists  (ShareSessionFactory *session_factory,
                                       ShareUrlString *url_string,
                                       gboolean *exists);

ShareStatus 
share_session_factory_session_managed (ShareSessionFactory *session_factory,
                                       ShareUrlString *url_string,
                                       gboolean *managed);

ShareStatus 
share_session_factory_list_sessions   (ShareSessionFactory *session_factory,
                                       ShareUrlString *names[]);

ShareStatus 
share_session_factory_list_sessions_on_host 
                                      (ShareSessionFactory *session_factory,
                                       gchar * *host,
                                       gchar * *connection_type,
                                       ShareUrlString *names[]);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SHARE_SESSION_FACTORY_H__ */
