dnl Process this file with autoconf to produce a configure script.

AC_INIT(configure.in)
AM_INIT_AUTOMAKE(Share, 0.2.10)
AM_CONFIG_HEADER(config.h)
AM_MAINTAINER_MODE

AC_ISC_POSIX
AC_PROG_CC
AM_PROG_CC_STDC
AC_HEADER_STDC
AM_PROG_LIBTOOL

pkg_modules="gtk+-2.0 >= 1.3.13"
PKG_CHECK_MODULES(PACKAGE, [$pkg_modules])
AC_SUBST(PACKAGE_CFLAGS)
AC_SUBST(PACKAGE_LIBS)

# i18n stuff
ALL_LINGUAS=""
AM_GLIB_GNU_GETTEXT
LIBS="$LIBS $INTLLIBS"

GLIB_PACKAGES="gobject-2.0 gmodule-2.0"
AC_SUBST(GLIB_PACKAGES)

GLIB_REQUIRED_VERSION=2.0.0
AC_SUBST(GLIB_REQUIRED_VERSION)

dnl This PATH_GLIB is somewhat redundant, but does a sanity compile and
dnl importantly defines the GLIB_GENMARSHAL variable for subst into the
dnl Makefile
AM_PATH_GLIB_2_0($GLIB_REQUIRED_VERSION, :,
  AC_MSG_ERROR([
*** GLIB $GLIB_REQUIRED_VERSION or better is required. The latest version of
*** GLIB is always available from ftp://ftp.gtk.org/. If GLIB is installed
*** but not in the same location as pkg-config add the location of the file
*** glib-2.0.pc to the environment variable PKG_CONFIG_PATH.]),
  gobject gmodule)

PKG_CHECK_MODULES(DEP, glib-2.0 >= $GLIB_REQUIRED_VERSION $GLIB_PACKAGES, ,
  AC_MSG_ERROR([
	*** GLib not found. You can find it on ftp://ftp.gtk.org
	*** Errors follow:
            $DEP_PKG_ERRORS]))

# Rerun PKG_CONFIG to add gthread-2.0 cflags, but not libs
DEP_CFLAGS=`$PKG_CONFIG --cflags $GLIB_PACKAGES gthread-2.0`

# define a MAINT-like variable REBUILD which is set if Perl
# and awk are found, so autogenerated sources can be rebuilt

AC_PROG_AWK
AC_CHECK_PROGS(PERL, perl5 perl)

REBUILD=\#
if test "x$enable_rebuilds" = "xyes" && \
	test -n "$PERL" && \
	$PERL -e 'exit !($] >= 5.002)' > /dev/null 2>&1 && \
	test -n "$AWK" ; then
  REBUILD=
fi
AC_SUBST(REBUILD)


# This is a check for gtk-doc which you can insert into your configure.in.
# You shouldn't need to change it at all.

##################################################
# Check for gtk-doc.
##################################################

AC_ARG_WITH(html-dir, [  --with-html-dir=PATH path to installed docs ])

if test "x$with_html_dir" = "x" ; then
  HTML_DIR='${datadir}/gtk-doc/html'
else
  HTML_DIR=$with_html_dir
fi

AC_SUBST(HTML_DIR)

gtk_doc_min_version=1.0
AC_MSG_CHECKING([gtk-doc version >= $gtk_doc_min_version])
if pkg-config --atleast-version=$gtk_doc_min_version gtk-doc; then
  AC_MSG_RESULT(yes)
  GTKDOC=true
else
  AC_MSG_RESULT(no)
  GTKDOC=false
fi

dnl Let people disable the gtk-doc stuff.
AC_ARG_ENABLE(gtk-doc, [  --enable-gtk-doc  Use gtk-doc to build documentation [default=auto]], enable_gtk_doc="$enableval", enable_gtk_doc=auto)

if test x$enable_gtk_doc = xauto ; then
  if test x$GTKDOC = xtrue ; then
    enable_gtk_doc=yes
  else
    enable_gtk_doc=no 
  fi
fi

AM_CONDITIONAL(ENABLE_GTK_DOC, test x$enable_gtk_doc = xyes)


AC_OUTPUT([
Makefile
share/Makefile
docs/Makefile
docs/reference/Makefile
])
