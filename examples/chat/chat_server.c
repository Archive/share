
/*
 * $Header$
 *
 * Copyright (c) 2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#include <stdio.h>
#include <share/share.h>


static gchar *
get_host(int argc, char *args[])
{
    int i;
    gchar *def_host = "localhost";
 
    for (i = 0; i < argc; i++) {
        if (!strcmp(args[i], "-server")) {
            if (++i < argc) {
                return(args[i]);
            }   
        }
    }    
 
    return(def_host);
}
 
 
static int
get_port(int argc, char *args[])
{
    int i;
    int def_port = 4461;
 
    for (i = 0; i < argc; i++) {
        if (!strcmp(args[i], "-port")) {
            if (++i < argc) {
                return(atoi(args[i]));
            }   
        }    
    }    
 
    return(def_port);
}
 
 
static gchar *
get_type(int argc, char *args[])
{
    int i;
    gchar *def_type = "socket";

    for (i = 0; i < argc; i++) {
        if (!strcmp(args[i], "-type")) {
            if (++i < argc) {
                return(args[i]);
            }
        }
    }    

    return(def_type);
}


int
main(int argc, char *argv[])
{
    ShareClient    *client       = NULL;
    ShareSession   *chat_session = NULL;
    ShareChannel   *chat_channel = NULL;
    gchar          *session_type = NULL;
    gchar          *host_name    = NULL;
    int            host_port     = 0;
    ShareUrlString *url          = NULL;
    ShareStatus    result        = SHARE_RESULT_OKAY;
    gboolean       exists        = FALSE;
    gchar          *session_name = NULL;
    gchar          *channel_name = NULL;

    sdt_init(argc, argv);
    session_name = "ChatSession";
    channel_name = "ChatChannel";
    session_type = get_type(argc, argv);
    host_name    = get_host(argc, argv);
    host_port    = get_port(argc, argv);
    url          = share_url_string_new_session_url(host_name, host_port, 
                                                    session_type, session_name);

    result = share_registry_factory_registry_exists(session_type, &exists);
    if (result == SHARE_RESULT_OKAY && exists == FALSE) {
        ShareRegistry *registry;

        registry = share_registry_new(session_type);
        if (registry != NULL) {
            result = share_registry_start_registry(registry);
            if (result == SHARE_RESULT_OKAY) {
                client = share_client_new("Server");
                chat_session = share_session_new_session(client, url, FALSE);
                if (chat_session != NULL) {
                    result = share_session_create_channel(chat_session, client, 
                                 channel_name, TRUE, TRUE, FALSE, chat_channel);
                    if (result == SHARE_RESULT_OKAY) {
                        printf("Setup and bound Chat server.\n");
                    } else {
                        fprintf(stderr, "chat_server: main: ");
                        fprintf(stderr, "unable to create the channel: %d\n",
                                result);
                    }
                } else {
                    fprintf(stderr, "chat_server: main: ");
                    fprintf(stderr, "unable to create the session\n");
                }
            } else {
                fprintf(stderr, "chat_server: main: ");
                fprintf(stderr, "unable to start the registry: %d\n", result);
            }
        } else {
            fprintf(stderr, "chat_server: main: ");
            fprintf(stderr, "unable to create the registry\n");
        }
    } else {
        fprintf(stderr, "chat_server: main: ");
        fprintf(stderr, "unable to check if registry exists: %d\n", result);
    }

    exit(0);
}
